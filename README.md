## Collaborative MR DataViz
NOTE: These instructions may be outdated. Please contact Benjamin directly should you want a demo.

### SandDance (codenamed BeachParty)
While I do have the source code for SandDance, there are a few issues with it in that it is an outdated version, is developed using Visual Studio 2012, and uses a very old version of TypeScript (v1.0.3.0). This is problematic when trying to build the solution, as a whole slew of compilation and syntax errors occur. Even with this fixed, runtime errors are still present which requires reverse engineering of how SandDance works to debug.

An easy solution is to simply use the actual files served by the SandDance website itself (can be found [here](https://sanddance2.azurewebsites.net/BeachPartyDemo/Welcome.html)). While this means that all the code which was once spread out across dozens of .ts files is now mushed into a few .js scripts, it is better than dealing with the aforementioned errors.

Note that as this is a browser application, you are required to host it yourself.

#### How to get it working
##### 1. Install Microsoft Internet Information Services (IIS)
1. Download and run [Microsoft Web Platform Installer](https://www.microsoft.com/web/downloads/platform.aspx)
2. Click on the "Products" tab and click on "Add" next to "IIS Recommended Configuration"
3. Click on "Install" at the bottom of the dialog
4. Navigate to Start | Control Panel | Uninstall a program | Turn Windows Features on/off | Internet Information Services | World Wide Web Services | Application Development Features. Make sure the following options are checked: .NET Extensibility 3.5; .NET Extensibility 4.7; ASP.NET 3.5; ASP.NET 4.7; ISAPI Extensions; and ISAPI Filters


##### 2. Configure IIS
1. Run "Internet Information Services (IIS) Manager"
2. Right click on the left "Connections" navigation panel and select "Add Website..."
3. Under "Site name" enter "bpServer"
4. Click on "Select..." on the right side of the dialog and under "Application pool:" select ".NET v4.5" and click "OK"
5. Under "Physical path" browse to the root directory of the BeachParty files
6. Leave the port as 80
7. Select "OK"
8. Look for and select the newly created bpServer node in the left "Connections" panel
9. Open "MIME Types" in the centre panel
10. For each of the following, Right click | "Add..." a new entry or modify the existing entry if it already exists and change the MIME Type of "text/plain": .txt; .csv; .json; .js; and .insights
11. Right click on the "bpServer" subfolder, select "Convert to Application", and click "OK"

##### 3. Configure permissions
1. With the "bpServer" site selected, double click on the "Authentication" icon in the center pane
2. Right click on "Anonymous Authentication" and select "Edit..."
3. Select the "Application pool identity" radial option and click "OK"
4. Navigate to the root BeachParty folder and open its properties menu
5. Switch to the "Security" tab and press "Edit..."
6. Click on "Add..."
7. Enter "IIS_IUSRS" in the text box and press "OK"
8. Ensure the newly added IIS_IUSRS group has at least "Read" permissions allowed (or allow "Full control" to be safe)
9. Keep clicking "OK" and exit from the properties window


### Displaying SandDance on a GameObject within Unity
If video pass-through is not available or if tracking issues due to screen reflectivity is an issue, the project contains a means of mirroring a webcam video source onto the object which represents the screen.

You will need to download and install [OBS Studio](https://obsproject.com/), the [OBS-VirtualCam plugin](https://github.com/CatxFish/obs-virtual-cam), and the required registry fix found in its [releases page](https://github.com/CatxFish/obs-virtual-cam/releases). Any other similar software can be used, as long as it can output to a virtual webcam (however the registry change for that specific software would be needed).

##### To configure OBS
1. Create a new Scene (if one is not already created) in the bottom Scenes panel
2. Add new Source to this Scene by clicking the + symbol of the Sources panel
3. Select Window Capture from the dropdown, give it any name you wish and click OK
4. Double click on the newly created Source to access its properties
5. From the Window dropdown, select the running .exe of the browser that will be running SandDance and click OK
6. The browser should now be visible in the main section of OBS. If the browser's video feed is not scaled correctly, simply click on it and adjust it accordingly

##### To run OBS-VirtualCam:
1. Click on Tools from the top menu options and select VirtualCam
2. Click on Start
3. That's it! The virtual camera will now be outputting the exact same video feed as the one shown in the main section of OBS

##### To configure for Unity:
1. Virtual webcams do not work natively with Unity as they lack a DevicePath registry entry. The easiest way to resolve this is to run the aforementioned registry fix
2. Unity may need to be restarted