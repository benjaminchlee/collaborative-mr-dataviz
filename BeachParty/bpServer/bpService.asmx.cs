﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Script.Services;
using System.Web.Services;

namespace bpServer
{
    /// <summary>
    /// API needed for SandDance App, version Feb-2016.
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)] 
    public class bpService : System.Web.Services.WebService
    {

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string HelloWorld()
        {
            return "Hello World!";
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string DownloadCsvAsJsonVectors(string url, string delimeter, bool hasHeader, bool inferTypes)
        {
            var reader = new WebClient();
            //reader.Headers.Add("accept", "application/json");

            //---- don't catch exceptions here; let them return to caller ----
            var str = reader.DownloadString(url);
            string jsonStr = null;

            //---- could be using a LARGE amount of memory; free it now ----
            reader.Dispose();

            //---- don't parse it if it is a JSON string already (vs. CSV/TXT) ----
            if ((str.StartsWith("[") && str.EndsWith("]")) || (str.StartsWith("{") && str.EndsWith("}")))
            {
                jsonStr = str;
            }
            else 
            {
                jsonStr = CsvQuickParser(str, hasHeader, delimeter[0]);
            }

            return jsonStr;
        }

        string BuildWhereClause(AggFilter[] aggFilters)
        {
            var str = "where ";
            var firstAf = true;

            foreach (var af in aggFilters)
            {
                if (!firstAf)
                {
                    str += "and ";
                }

                str += "(";
                if (!af.isMember)
                {
                    str += "not ";
                }

                str += af.colName + " in (";
                var firstValue = true;

                foreach (var value in af.values)
                {
                    if (!firstValue)
                    {
                        str += ", ";
                    }

                    str += "'" + value + "'";
                    firstValue = false;
                }

                str += "))";

                firstAf = false;
            }

            return str;
        }

        bool IsOnlyId(string str)
        {
            var isId = true;

            foreach (char ch in str)
            {
                if (!(Char.IsLetterOrDigit(ch) || ch == '_'))
                {
                    isId = false;
                    break;
                }
            }

            return isId;
        }


        [WebMethod]
        public string writeSessionFile()  /* string userName, string fileName, string contents */
        {
            //---- not sure why, but "Context.Request.Form" returns no strings, so we get full body & parse ----
            StreamReader reader = new StreamReader(Context.Request.InputStream);
            string body = reader.ReadToEnd();
            body = HttpUtility.UrlDecode(body);

            //---- parse "body" into name=value pairs: fn=xxx&text=ttt ----
            var pairs = body.Split('&');
            var userName = pairs[0].Substring(9);
            var fileName = pairs[1].Substring(9);
            var contents = pairs[2].Substring(9);

            return writeSessionFileCore(userName, fileName, contents);
        }

        string writeSessionFileCore(string userName, string fileName, string contents)
        { 
            var relPath = @"\Shared\";

            if (userName != null && userName != "")
            {
                relPath += userName;
            }
            else
            {
                relPath += "Root";
            }

            if (fileName == null || fileName == "")
            {
                fileName = Guid.NewGuid().ToString();
            }

            relPath += @"\" + fileName;
            var path = GetSessionsDir() + "\\" + relPath;

            //---- we encoded this before sending it, so we must decode now ----
            //contents = HttpUtility.UrlDecode(contents);

            var dir = System.IO.Path.GetDirectoryName(path);
            if (!Directory.Exists(dir))
            {
                Directory.CreateDirectory(dir);
            }

            File.WriteAllText(path, contents);

            return relPath;
        }

        [WebMethod]
        public string publishSessionToWebPage()  /* string userName, string fileName, string contents */
        {
            //---- not sure why, but "Context.Request.Form" returns no strings, so we get full body & parse ----
            StreamReader reader = new StreamReader(Context.Request.InputStream);
            string body = reader.ReadToEnd();
            body = HttpUtility.UrlDecode(body);

            //---- parse "body" into name=value pairs: fn=xxx&text=ttt ----
            var pairs = body.Split('&');
            var userName = pairs[0].Substring(9);
            var dirName = pairs[1].Substring(9);
            var contents = pairs[2].Substring(9);
            var bpUrl = pairs[3].Substring(6);

            var sessionUrl = writeSessionFileCore(null, null, contents);

            var guid = Guid.NewGuid().ToString();
            var dir = GetPagesDir();
            var path = dir + @"\Public\" + guid;

            //---- we encoded this before sending it, so we must decode now ----
            //contents = HttpUtility.UrlDecode(contents);

            Directory.CreateDirectory(path);

            //---- first file: the insights JSON ----
            var fnInsights = path + "\\insights.json";
            File.WriteAllText(fnInsights, contents);

            //---- convert from JSON ----
            JavaScriptSerializer jss = new JavaScriptSerializer();
            jss.MaxJsonLength = 100 * 1000 * 1000;
            var insightFile = jss.Deserialize<InsightFile>(contents);

            //---- the target when HTML page is clicked ----
            var targetUrl = bpUrl + "?session=" + sessionUrl;

            //---- second file: the HTML file ----
            var html = CreateWebPage(insightFile.insights, targetUrl);
            var fnHtml = path + "\\default.html";
            File.WriteAllText(fnHtml, html);

            //---- return the relative URL for this webpage ----
            var url = "SandDancePages/Public/" + guid + "/default.html";

            return url;
        }

        string CreateWebPage(InsightData[] insights, string url)
        {
            var htmlPre = @"
<!DOCTYPE html>
<html xmlns='http://www.w3.org/1999/xhtml'
< head>
     <meta http-equiv='X-UA-Compatible' content='IE=Edge' />
     <title>SandDance Insights</title>
	 <meta charset='utf-8' />

    <style>

body{background:#FFFFFF;color:black;font-family:Calibri;font-size:16px;}
a{
font-weight:bold;
text-decoration:none;
font-size:16px;
}

div{
font-size:36px;
font-weight:bold;
text-align:center;
}
th,td{
padding-bottom:20px;
border-bottom:1px solid black;
border-left:1px solid black;
padding:0 5px;
}

td.description{
padding:20px;
text-align:left;
}

table{
border:1px solid black;
border-collapse:
collapse;
}

td.name{
border-bottom:none;
}

 /* td {border: 1px solid black; cursor: pointer;}
        .name {padding: 8px; max-width: 200px;}
        .description {padding: 8px; }
 */

    </style>
</head>
<body>
    <div>SandDance Insights</div>
    <br />
    <br />
    <div>
        <table>
";

            var htmlPost = @"
        </table>
    </div>
</body>
</html>";

            var htmlEntry = @"
 <tr>
                <td class='name'><a href='@href'>@name</a></td>
                <td class='description' rowspan='2' valign='top'><a href='@href'>@desc</a></td>
            </tr>
            <tr>
                <td class='image'>
                    <a href='@href'><img src='@imgSrc' style='width: 200px; height: 200px; '/></a>
                </td>
            </tr>";

            var sb = new StringBuilder();

            foreach (var insight in insights)
            {
                var entry = htmlEntry.Replace("@name", insight.name);
                entry = entry.Replace("@desc", insight.notes);
                entry = entry.Replace("@imgSrc", insight.imageAsUrl);
                entry = entry.Replace("@href", url);

                sb.AppendLine(entry);
            }

            var html = htmlPre + sb.ToString() + htmlPost;
            return html;
        }

        [WebMethod]
        public string readSessionFile(string sessionUrl)
        {
            var path = GetSessionsDir() + sessionUrl;
            var str = File.ReadAllText(path);     
            return str;
        }

        string GetPagesDir()
        {
            string dir = null;
            var myHostName = HttpContext.Current.Request.Url.Host;

            if (myHostName.ToLower().Contains("azurewebsites"))
            {
                dir = @"d:\home\site\wwwroot\SandDancePages";
            }
            else
            {
                dir = GetSessionsDir() + @"\WebPages";
            }

            if (!Directory.Exists(dir))
            {
                Directory.CreateDirectory(dir);
            }

            return dir;
        }

        string GetSessionsDir()
        {
            string dir = null;
            var myHostName = HttpContext.Current.Request.Url.Host;

            if (myHostName.ToLower().Contains("azurewebsites"))
            {
                //---- these environment variables seem to be undefined ----
                //dir = Environment.ExpandEnvironmentVariables("HOME");
                //dir = Environment.ExpandEnvironmentVariables("HOMEDRIVE");
                //dir += Environment.ExpandEnvironmentVariables("HOMEPATH");
                dir = @"d:\home\\BeachParty";
            }
            else
            { 
                dir = Properties.Settings.Default.sessionsDir;
            }

            return dir;
        }

        static string CsvQuickParser(string str, bool isFirstRowHeader, char delimiter)
        {
            List<NamedVector> namedVectors = new List<NamedVector>();

            var lines = str.Split(new string[] { "\r\n" }, StringSplitOptions.RemoveEmptyEntries);
            if (lines.Length == 1)
            {
                //---- try just a \n ----
                lines = str.Split('\n');
            }
            if (lines.Length == 1)
            {
                //---- try just a \r ----
                lines = str.Split('\r');
            }

            //---- parse lines ----
            var lineNum = 0;
            for (var i = 0; i < lines.Length; i++)
            {
                var line = lines[i];
                ParseCsvLine(namedVectors, line, delimiter, isFirstRowHeader, lineNum++);
            }

            //---- convert into Dict<> before JSON ----
            Dictionary<string, string[]> dataFrame = new Dictionary<string, string[]>();

            for (var i = 0; i < namedVectors.Count; i++)
            {
                var nv = namedVectors[i];
                dataFrame[nv.name] = nv.vector.ToArray();
            }

            JavaScriptSerializer jss = new JavaScriptSerializer();
            jss.MaxJsonLength = 100 * 1000 * 1000;

            var json = jss.Serialize(dataFrame);

            return json;
        }

        static void ParseCsvLine(List<NamedVector> namedVectors, string line, char delimiter, bool isFirstRowHeader, int lineNum)
        {
            var i = 0;
            var field = "";
            var inQuotedString = false;
            int fieldIndex = 0;

            //---- parse fields in line ----
            while (i < line.Length)
            {
                var ch = line[i++];

                if (ch == delimiter && !inQuotedString)
                {
                    //---- start new field ----
                    AddField(namedVectors, field, fieldIndex++, lineNum, isFirstRowHeader);
                    field = "";
                }
                else if (ch == '"')
                {
                    //---- QUOTE - is it at the start of a field? ----
                    if (field == "")
                    {
                        inQuotedString = true;
                    }
                    else if (inQuotedString)
                    {
                        //---- peek ahead to next char ----
                        var nextCh = (i < line.Length) ? line[i] : ' ';

                        if (nextCh == '"')      // double quote 
                        {
                            field += '"';       // add a single quote
                            i++;                // we used peek char
                        }
                        else
                        {
                            //---- must be ending quote ----
                            inQuotedString = false;
                        }
                    }
                }
                else
                {
                    field += ch;
                }
            }

            //---- add last field ----
            AddField(namedVectors, field, fieldIndex++, lineNum, isFirstRowHeader);
        }

        static void AddField(List<NamedVector> namedVectors, string field, int fieldIndex, int lineNum, bool isFirstRowHeader)
        {
            if (lineNum == 0)
            {
                //---- add as key ----
                var colName = (isFirstRowHeader) ? field : ("Col" + (fieldIndex + 1));

                var nv = new NamedVector(colName);
                namedVectors.Add(nv);
            }
            else
            {
                var nv = namedVectors[fieldIndex];
                nv.vector.Add(field);
            }
        }

    }

    public class InsightData
    {
        public string name;
        public string notes;
        public string imageAsUrl;
    }

    public class InsightFile
    {
        public double version;
        public InsightData[] insights;
    }

    public class AggFilter
    {
        public string colName;
        public bool isMember;
        public string[] values;
    }

    public class NamedVector
    {
        public string name;
        public List<string> vector;

        public NamedVector(string name)
        {
            this.name = name;
            this.vector = new List<string>();
        }
    }

    public class AggResult
    {
        public bool wasSampled;
        public bool wasAggregated;
        public long trueRecordCount;
        public long aggFilteredRecordCount;

        public Dictionary<string, string[]> data;
    }

}
