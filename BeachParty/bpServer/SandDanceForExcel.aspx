<%@ Page Language="C#" %>
<%
    // This is just a redirect so that we can change where the Excel add-in window 
    // navigates to without having to release a new build of the add-in.
    // Note: We can only add URL params here, not on the Start.aspx page, because
    //       that page will do a Server.Transfer, which cannot add query parameters.
    // Note: Start.aspx has to live in the same folder as SandDance.html in order
    //       to not break all the relative script paths in SandDance.html.
    Response.Redirect("../testlib/SandDance/Start.aspx");
%>