﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;

namespace bpServer
{
    /// <summary>
    /// FastTemplateServer - helper class for building HTML pages from a template and a data stream.
    /// </summary>
    
    public class FastTemplateServer
    {
        static int nextShapeSheetId = 0;

        Bitmap _canvas;
        Dictionary<string, object>[] _selectSets;
        Dictionary<string, object> _currentShapeSheet = null;
        Graphics _ctx;

        public Dictionary<string, object> BuildCardShapeSheet(cmdTemplate template, int mySize, Dictionary<string, object>[] jsonData, 
            string baseImagesUrl, Dictionary<string, object>[] selectSets, int[] uiState, string fnShapeSheet)
        {
            var start = DateTime.Now;

            this._selectSets = selectSets;

            //---- build shapes to match current shape size and scaleFactor ----

            var cellWidth = mySize;
            var cellHeight = mySize;

            //---- try to build as a square texture, to avoid hitting texture limit problems of a sinble row ----
            var shapeCount = jsonData.Length;
            var colCount = (int)Math.Floor(Math.Sqrt(shapeCount));
            var rowCount = (int)Math.Ceiling((double)shapeCount / colCount);

            var shapeSheetId = nextShapeSheetId++;

            var sheetWidth = cellWidth * colCount;
            var sheetHeight = cellHeight * rowCount;

            var needRendering = (! File.Exists(fnShapeSheet));
            if (needRendering)
            {
                Dictionary<string, object> record = null;

                var cx = 0;
                var cy = 0;

                Graphics ctx = null;
                Bitmap canvas = null;

                this._currentShapeSheet = null;

                //---- create a CANVAS to hold this shapeSheet ----
                canvas = new Bitmap(sheetWidth, sheetHeight);
                this._canvas = canvas;

                ctx = Graphics.FromImage(canvas);
                this._ctx = ctx;

                var font = new Font("Tahoma", 8, GraphicsUnit.Point);
                var firstShape = true;

                //---- create context for drawing and filling template values ----
                var fillContext = new templateContext();
                fillContext.data = jsonData;
                fillContext.baseImagesUrl = baseImagesUrl;
                fillContext.drawingSize = mySize;
                
                //---- draw each shape onto texture ----
                for (var s = 0; s < shapeCount; s++)
                {
                    if (s >= jsonData.Length)
                    {
                        break;
                    }

                    //---- cannot skip filtered items because it makes our shapesheet NON-REUSABLE ----
                    //---- save time - only create cards for shapes in the filter ----
                    //---- (but we still must step thru loop to keep the cx,cy offsets matching all records ----
                    if (true)           // uiState[s] != (int)uiStateEnum.filteredOutOfLayout)
                    {
                        //---- CLEAR CARD ----
                        var cr = ToColor(template.backgroundColor);
                        var br = new SolidBrush(cr);

                        ctx.FillRectangle(br, new Rectangle(cx, cy, cellWidth, cellHeight));

                        //---- do NOT draw borders on server ----

                        //---- draw border around INSIDE of card ----
                        var borderSize = 2;

                        //---- to make images more reusable, only draw borders on the client ----
                        //var borderColor = this.GetBorderColor(s, uiState);

                        ////---- adjustments here draw the border on inside of card edges, rather than centered on the edges ----
                        //var pen = new Pen(ToColor(borderColor), borderSize);
                        //ctx.DrawRectangle(pen, cx + borderSize / 2, cy + borderSize / 2, cellWidth - borderSize, cellHeight - borderSize);

                        var x = cx + borderSize;
                        var y = cy + borderSize;
                        var cw = cellWidth - 2 * borderSize;
                        var ch = cellHeight - 2 * borderSize;

                        record = jsonData[s];

                        //---- add record context ----
                        fillContext.dataIndex = s;
                        fillContext.record = record;

                        var drawCmds = template.drawCmds;
                        for (var i = 0; i < drawCmds.Length; i++)
                        {
                            var drawCmd = drawCmds[i];
                            Rectangle rcBoundsFilled;

                            if (firstShape)
                            {
                                //---- create this once, using first record context ----
                                rcBoundsFilled = this.fillBoundsTemplates(drawCmd.bounds, fillContext);
                                drawCmd.filledBounds = rcBoundsFilled;
                            }
                            else
                            {
                                rcBoundsFilled = drawCmd.filledBounds;
                            }

                            //---- copy rectangle so don't overwrite the original ----
                            var rc = new Rectangle(rcBoundsFilled.Left, rcBoundsFilled.Top,
                                rcBoundsFilled.Width, rcBoundsFilled.Height);

                            rc.Offset(x, y);

                            if (drawCmd.type == "image")
                            {
                                this.drawImage(drawCmd, rc, fillContext);
                            }
                            else if (drawCmd.type == "text")
                            {
                                this.drawText(drawCmd, ctx, rc, fillContext);
                            }
                        }

                        firstShape = false;      // we have now drawn our first image 

                        //hostServices.debug("drawing '" + text + "' at cx=" + cx + ", cy=" + cy);
                    }

                    cx += cellWidth;
                    if (cx >= sheetWidth)
                    {
                        cx = 0;
                        cy += cellHeight;
                    }
                }

                //---- write the shapeSheet to a server file ----
                canvas.Save(fnShapeSheet, ImageFormat.Png);

                var elapsed = DateTime.Now - start;

                Debug.WriteLine("buildShapeSheet elapsed=" + elapsed.TotalMilliseconds + " ms, size=" + mySize + ", shapeCount=" + shapeCount +
                    ", rowCount=" + rowCount + ", colCount=" + colCount +
                    ", sheet.width = " + sheetWidth + ", sheet.height = " + sheetHeight);

                //---- release _ctx resource ----
                if (this._ctx != null)
                {
                    this._ctx.Dispose();
                    this._ctx = null;

                    this._canvas.Dispose();
                    this._canvas = null;
                }

            }

            //---- build return hash object ---
            Dictionary<string, object> ss = new Dictionary<string, object>();

            ss["symbolCount"] = colCount;
            ss["symbolWidth"] = cellWidth;
            ss["symbolHeight"] = cellHeight;

            ss["totalColorCount"] = rowCount;
            ss["fromColorCount"] = 0;
            ss["toColorCount"] = rowCount;

            ss["uiColorCount"] = 1;
            //ss["base64"] = ToBase64(canvas);     // don't try to see this value in VS (it hangs)
            ss["shapeSheetId"] = shapeSheetId;

            this._currentShapeSheet = ss;

            return ss;
        }

        Rectangle fillBoundsTemplates(Dictionary<string,object> rc, templateContext fillContext)
        {
            //---- don't overwrite original template values ----
            var left = rc["left"];
            var top = rc["top"];
            var width = rc["width"];
            var height = rc["height"];

            if (left is string)
            {
                left = fill((string)left, fillContext);
            }

            if (top is string)
            {
                top = fill((string)top, fillContext);
            }

            if (width is string)
            {
                width = fill((string)width, fillContext);
            }

            if (height is string)
            {
                height = fill((string)height, fillContext);
            }

            var rcx = new Rectangle(ToInt(left), ToInt(top), ToInt(width), ToInt(height));
            return rcx;
        }

        int ToInt(object value)
        {
            int iValue = Convert.ToInt32(value);
            return iValue;
        }

        string fill(string str, templateContext fillContext)
        {
            var newStr = templateFillerServer.fill(str, fillContext);
            return newStr;
        }

        void drawImage(drawCmd drawCmd, Rectangle rc, templateContext fillContext)
        {
            //---- draw image ----
            var imageSrc = drawCmd.imageSrc;
            imageSrc = fill(imageSrc, fillContext);

            if (imageSrc != null && imageSrc.Length > 0)
            {
                //---- Bitmap ctr doesn't support URI's ----
                //var img = new Bitmap(imageSrc);
                Bitmap img = null;
                var wc = new WebClient();

                try
                {
                    using (var stream = wc.OpenRead(imageSrc))
                    {
                        img = (Bitmap)Image.FromStream(stream);
                    }

                    DrawLoadedImage(img, rc);

                    img.Dispose();
                }
                catch (Exception ex)
                {
                    Debug.WriteLine("Error download imageSrc=" + imageSrc + ", error=" + ex.Message);
                    //---- skip over images that we cannot download and draw ----
                }
            }
        }

        void drawText(drawCmd drawCmd, Graphics ctx, Rectangle rc, templateContext fillContext)
        {
            var value = drawCmd.value;
            value = fill(value, fillContext);

            var myFont = ToFont(drawCmd.font);

            //ctx.font = font;
            //ctx.fillStyle = drawCmd.color;

            //---- always measure text (for alignment & potential clipping) ----
            var text = value;
            Brush br = new SolidBrush(ToColor(drawCmd.color));

            var sz = ctx.MeasureString(text, myFont);

            var left = rc.Left;
            var top = rc.Top + 0;       // 0: baseline offset for drawing on server

            var clipped = false;

            var fontHeight = sz.Height;

            var diff = rc.Width - sz.Width;
            if (diff < 0)
            {
                //---- need to apply clipping  ----
                ctx.Clip = new Region(new Rectangle(left, top, rc.Width, rc.Height));
                clipped = true;
            }
            else
            {
                var hAdjust = (int)this.CalculateHAlign(rc.Width, sz.Width, drawCmd.hAlign);
                left += hAdjust;
            }

            var vAdjust = (int)CalculateVAlign(rc.Height, fontHeight, drawCmd.vAlign);
            top += vAdjust;

            //---- draw NAME text ----
            ctx.DrawString(text, myFont, br, new PointF(left, top));

            if (clipped)
            {
                ctx.ResetClip();
            }
        }

        Font ToFont(string fontDesc)
        {
            string family = "Tahoma";
            FontStyle style = FontStyle.Regular;
            float size = 8;
            GraphicsUnit units = GraphicsUnit.Point;

            if (fontDesc != null && fontDesc.Length > 0)
            {
                fontDesc = fontDesc.Replace(",", " ");
                var parts = fontDesc.Split(' ');

                for (var i = 0; i < parts.Length; i++)
                {
                    var part = parts[i];

                    part = part.ToLower();
                    if (part == "italic")
                    {
                        style = FontStyle.Italic;
                    }
                    else if (part == "bold")
                    {
                        style = FontStyle.Bold;
                    }
                    else if (part.Length > 0 && Char.IsDigit(part[0]))
                    {
                        //---- starts with a number; must be a size ----
                        //---- separate number from units ----
                        int unitsStart = -1;
                        for (var c = 1; c < part.Length; c++)
                        {
                            if (!Char.IsDigit(part[c]))
                            {
                                unitsStart = c;
                                break;
                            }
                        }

                        string unitStr = "";
                        string numStr = part;

                        if (unitsStart > -1)
                        {
                            unitStr = part.Substring(unitsStart).Trim();
                            numStr = part.Substring(0, unitsStart).Trim();
                        }

                        size = float.Parse(numStr);

                        if (unitStr != "" && unitStr != "pt")
                        {
                            if (unitStr == "px")
                            {
                                units = GraphicsUnit.Pixel;
                            }
                            else
                            {
                                //---- ignore for now (maybe issue error in future) ----
                            }
                        }
                    }
                    else
                    {
                        //--- must be the font family ----
                        family = part;

                        //---- get rest of name ----
                        for (var j = i + 1; j < parts.Length; j++)
                        {
                            family += " " + parts[j];
                        }

                        break;
                    }
                }
            }

            var font = new Font(family, size, style, units);
            return font;
        }

        void DrawLoadedImage(Bitmap img, Rectangle rc)
        {
            var imgWidth = img.Width;
            var imgHeight = img.Height;

            var availWidth = rc.Width;
            var availHeight = rc.Height;

            //---- preserve aspect ratio ----
            float wRatio = availWidth / (float)imgWidth;
            float hRatio = availHeight / (float)imgHeight;

            float drawWidth = imgWidth;
            float drawHeight = imgHeight;

            if ((wRatio < 1) || (hRatio < 1))
            {
                //---- must scale down ----
                if (wRatio < hRatio)
                {
                    drawWidth *= wRatio;
                    drawHeight *= wRatio;
                }
                else
                {
                    drawWidth *= hRatio;
                    drawHeight *= hRatio;
                }
            }

            var xAlignment = (int)this.CalculateHAlign(availWidth, drawWidth, hAlignEnum.center);
            var yAlignment = (int)this.CalculateVAlign(availHeight, drawHeight, vAlignEnum.center);

            this._ctx.DrawImage(img, rc.Left + xAlignment, rc.Top + yAlignment, drawWidth, drawHeight);
        }

        double CalculateHAlign(double totalWidth, double elemWidth, hAlignEnum hAlign)
        {
            var adjust = 0.0;
            var diff = totalWidth - elemWidth;

            if (hAlign == hAlignEnum.center)
            {
                adjust = diff / 2;
            }
            else if (hAlign == hAlignEnum.right)
            {
                adjust = diff;
            }

            return adjust;
        }

        double CalculateVAlign(double totalHeight, double elemHeight, vAlignEnum vAlign)
        {
            var adjust = 0.0;
            var diff = totalHeight - elemHeight;

            if (vAlign == vAlignEnum.center)
            {
                adjust = diff / 2;
            }
            else if (vAlign == vAlignEnum.bottom)
            {
                adjust = diff;
            }

            return adjust;
        }

        //string ExpandFields(string value, Dictionary<string, object> record, int dataIndex, string baseUrl)
        //{
        //    if (value.StartsWith("@"))
        //    {
        //        var colName = value.Substring(1);
        //        value = (string)record[colName];
        //    }
        //    else
        //    {
        //        value = value.Replace("%dataIndex%", dataIndex + "");
        //        value = value.Replace("%baseImagesUrl%", baseUrl);
        //    }

        //    return value;
        //}

        int GetBestImgSize(int mySize)
        {
            var imgSize = 0;

            if (mySize > 1024)
            {
                imgSize = 2048;
            }
            else if (mySize > 512)
            {
                imgSize = 1024;
            }
            else if (mySize > 256)
            {
                imgSize = 512;
            }
            else if (mySize > 128)
            {
                imgSize = 256;
            }
            else if (mySize > 64)
            {
                imgSize = 128;
            }
            else if (mySize > 32)
            {
                imgSize = 64;
            }
            else if (mySize > 16)
            {
                imgSize = 32;
            }
            else if (mySize > 8)
            {
                imgSize = 16;
            }
            else if (mySize > 4)
            {
                imgSize = 8;
            }
            else if (mySize > 2)
            {
                imgSize = 4;
            }
            else if (mySize > 1)
            {
                imgSize = 2;
            }
            else
            {
                imgSize = 1;
            }

            return imgSize;
        }

        string ToBase64(Bitmap bmp)
        {
            var base64 = "";

            using (var ms = new MemoryStream())
            {
                bmp.Save(ms, System.Drawing.Imaging.ImageFormat.Bmp);
                var bytes = ms.ToArray();

                base64 = Convert.ToBase64String(bytes);
            }

            return base64;
        }

        Color ToColor(string str)
        {
            str = Char.ToUpper(str[0]) + str.Substring(1);

            Color cr = Color.FromName(str);
            return cr;
        }

        string GetBorderColor(int shapeIndex, int[] uiState)
        {
            var uis = uiState[shapeIndex];
            var isSelected = (uis == (int)uiStateEnum.selected || uis == (int)uiStateEnum.hoverSelected);

            var cr = "black";
            if (isSelected)
            {
                cr = "red";
            }
            else
            {
                //---- see if this shape is a member of a selection set ----
                var selectSets = this._selectSets;

                for (var i = 0; i < selectSets.Length; i++)
                {
                    var set = selectSets[i];
                    if (IsMember(shapeIndex, (string)set["selection"]))
                    {
                        cr = (string)set["color"];
                        break;
                    }
                }
            }

            //this._selectSets

            return cr;
        }

        Boolean IsMember(int shapeIndex, string str)
        {
            var member = false;

            var invert = str.StartsWith("!");
            if (invert)
            {
                str = str.Substring(1);
            }

            var nums = str.Split(',');
            string shapeIndexStr = shapeIndex + "";

            if (Array.IndexOf(nums, shapeIndexStr) > -1)
            {
                member = (!invert);
            }

            return member;
        }


       
    }

    public enum uiStateEnum
    {
        normal,                 // 0     unselected and no hover
        hover,                  // 1
        hoverSelected,          // 2
        selected,               // 3
        filteredOut,            // 4   (if you add a value, update "uiStateCount" below)
        filteredOutOfLayout,    // 5   (filter out and out of shader's layout)
    }

    public enum hAlignEnum
    {
        left,
        center,
        right
    }

   public enum vAlignEnum
   {
        top,
        center,
        bottom
    }

    public class drawCmd
    {
        public string type;
        public hAlignEnum hAlign;
        public vAlignEnum vAlign;
        public Dictionary<string, object> bounds;
        public double opacity;

        //---- for c# casting, we include all fields in this base class ----
        public string value;
        public string font;
        public string color;
        public string imageSrc;

        //---- when loaded in memory ----
        public double fontHeight;

        //---- when loaded in memory ----
        public Rectangle filledBounds;
    }

    public class cmdTemplate
    {
        public string name;
        public string description;
        public string backgroundColor;

        public drawCmd[] drawCmds;
    }

    public class templateContext
    {
        public Dictionary<string, object> record;
        public Dictionary<string, object>[] data;
        public int dataIndex;
        public string baseImagesUrl;
        public double drawingSize;
        public Boolean isUnsafe;        // if true, encode for safe HTML content
    }

    public class paramsHash
    {
        public string dataName;
        public cmdTemplate template;
        public double mySize;
        public Dictionary<string, object>[] jsonData;
        public string baseImagesUrl;
        public Dictionary<string, object>[] selectSets;
        public string uiStateStr;
    }
}