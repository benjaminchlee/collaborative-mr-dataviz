﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;

namespace bpServer
{
    /// <summary>
    /// FileSlicer - helper class for building and serving data slices.
    /// </summary>
    
    public class FileSlicer
    {
        public static string buildFullSliceSheet(string modelName, string tableName, string imageName, int sliceSize,
            string[] imageUrls, bool forceIfExists, string baseFolder)
        {
            var status = "not built";

            var createdDir = false;

            if (!isValidSliceSize(sliceSize))
            {
                Error("Illegal sliceSize: " + sliceSize);
            }

            //---- create our folder to hold the sheet or files ----
            var sliceFolder = GetSliceFolderLocal(baseFolder, modelName, tableName, imageName, sliceSize);
            if (!Directory.Exists(sliceFolder))
            {
                Directory.CreateDirectory(sliceFolder);
                createdDir = true;
            }
            else
            {
                //---- remove all files ----
                Directory.Delete(sliceFolder, true);
                Directory.CreateDirectory(sliceFolder);
            }


            var imageCount = imageUrls.Length;
            var writeCount = 0;
            var errorCount = 0;

            if ((createdDir) || (forceIfExists))
            {
                var useSheet = true;            // todo: make this conditional on imageCount and sliceSIze  calculation

                var colCount = (int)Math.Floor(Math.Sqrt(imageCount));
                var rowCount = (int)Math.Ceiling(imageCount / (double)colCount);

                if (useSheet)
                {
                    //---- write each image to a single sheet file ----
                    var sheetWidth = colCount * sliceSize;
                    var sheetHeight = rowCount * sliceSize;

                    var sheet = new Bitmap(sheetWidth, sheetHeight);
                    using (Graphics g = Graphics.FromImage(sheet))
                    {
                        int x = 0;
                        int y = 0;

                        foreach (string url in imageUrls)
                        {
                            var wc = new WebClient();

                            try
                            {
                                using (var stream = wc.OpenRead(url))
                                {
                                    using (var img = (Bitmap)Image.FromStream(stream))
                                    {
                                        g.DrawImage(img, new Rectangle(x, y, sliceSize, sliceSize));
                                        writeCount++;
                                    }
                                }

                            }
                            catch (Exception ex)
                            {
                                Debug.WriteLine("Error download imageSrc=" + url + ", error=" + ex.Message);
                                //---- skip over images that we cannot download and draw ----
                                errorCount++;
                            }

                            x += sliceSize;

                            if (x >= sheetWidth)
                            {
                                x = 0;
                                y += sliceSize;
                            }
                        }
                    }

                    //---- finally, write sheet to file ----
                    var fn = sliceFolder +  "\\images-" + imageCount + ".png";

                    sheet.Save(fn, System.Drawing.Imaging.ImageFormat.Png);
                    sheet.Dispose();

                    status = "built: " + writeCount + " images, " + errorCount + " errors in SHEET";
                }
                else
                {
                    //---- write each image to server file of the specified sliceSize ----
                    int imageIndex = 0;

                    foreach (string url in imageUrls)
                    {
                        var fn = "file-" + imageIndex + ".png";
                        var written = SliceFile(url, fn, sliceSize);

                        if (written)
                        {
                            writeCount++;
                        }
                        else
                        {
                            errorCount++;
                        }

                        imageIndex++;
                    }

                    status = "built: " + writeCount + " images, " + errorCount + " errors as FILES";
                }
            }

            return status;
        }

        static bool SliceFile(string url, string fn, int sliceSize)
        {
            var wc = new WebClient();
            bool written = false;

            try
            {
                using (var stream = wc.OpenRead(url))
                {
                    using (var img = (Bitmap)Image.FromStream(stream))
                    {
                        var imgWidth = img.Width;
                        var imgHeight = img.Height;

                        var drawWidth = imgWidth;
                        var drawHeight = imgHeight;

                        var maxDim = Math.Max(imgWidth, imgHeight);
                        if (maxDim > sliceSize)
                        {
                            var ratio = sliceSize / (double)maxDim;

                            drawWidth = (int)Math.Round(drawWidth * ratio);
                            drawHeight = (int)Math.Round(drawHeight * ratio); ;
                        }

                        var bmp = new Bitmap(drawWidth, drawHeight);

                        using (Graphics g = Graphics.FromImage(bmp))
                        {
                            g.DrawImage(img, new Rectangle(0, 0, drawWidth, drawHeight));
                        }

                        bmp.Save(fn, System.Drawing.Imaging.ImageFormat.Png);
                        bmp.Dispose();

                        written = true;
                    }
                }

            }
            catch (Exception ex)
            {
                Debug.WriteLine("Error download imageSrc=" + url + ", error=" + ex.Message);
                //---- skip over images that we cannot download and draw ----
            }

            return written;
        }

        public static int[] buildImageSlices(string sliceFolder, int sliceSize,
            string[] imageUrls, int[] imageIndexes)
        {
            int[] sliceNums = new int[imageUrls.Length];

            if (!isValidSliceSize(sliceSize))
            {
                Error("Illegal sliceSize: " + sliceSize);
            }

            DirectoryInfo di = new DirectoryInfo(sliceFolder);

            if (!di.Exists)
            {
                di.Create();
            }

            for(var i=0; i < imageIndexes.Length; i++)
            {
                var url = imageUrls[i];
                if (url == null || url == "")
                {
                    sliceNums[i] = 0;
                }
                else
                {
                    var imageIndex = imageIndexes[i];
                    var fn = sliceFolder + "/f" + imageIndex + ".png";

                    if (File.Exists(fn))
                    {
                        FileInfo fi = new FileInfo(fn);
                        var sliceNum = (fi.Length > 0) ? sliceSize : 0;
                        sliceNums[i] = sliceNum;
                    }
                    else
                    {
                        //---- create the requested slice image file ----
                        var written = SliceFile(url, fn, sliceSize);

                        var sliceNum = (written) ? sliceSize : 0;
                        sliceNums[i] = sliceSize;
                    }
                }
            }

            return sliceNums;
        }

        static FileInfo GetImagesFile(string folderName)
        {
            FileInfo fi = null;
            
            DirectoryInfo di = new DirectoryInfo(folderName);
            foreach (FileInfo fix in di.GetFiles("images*.png"))
            {
                //----- take first match ----
                fi = fix;
                break;
            }

            return fi;
        }

        public static string getFullSliceSheet(string modelName, string tableName, string colName, int sliceSize, string baseFolder)
        {
            var sliceFolder = GetSliceFolderLocal(baseFolder, modelName, tableName, colName, sliceSize);
            var fi = GetImagesFile(sliceFolder);
            if (fi == null)
            {
                Error("Images*.png slice file not found in folder: " + sliceFolder);
            }

            //Bitmap bmp = new Bitmap(fi.FullName);
            //var base64 = ToBase64(bmp);
            //bmp.Dispose();
            var url = GetSlicerFolderURL(baseFolder, modelName, tableName, colName, sliceSize) + "/" + fi.Name;

            return url;     // base64;
        }

        public static string getPartialSliceSheet(string modelName, string tableName, string colName, int sliceSize,
            string recordNumbers, string baseFolder)
        {
            var sliceFolder = GetSliceFolderLocal(baseFolder, modelName, tableName, colName, sliceSize);
            var fnInputSheet = sliceFolder + "\\fullSheet.png";

            if (!File.Exists(fnInputSheet))
            {
                Error("Input sheet file not found: " + fnInputSheet);
            }

            Bitmap bmpInput = new Bitmap(fnInputSheet);
            var fromColCount = bmpInput.Width / sliceSize;
            var fromRowCount = bmpInput.Height / sliceSize;
            var maxFromCount = fromColCount * fromRowCount;

            var imageCount = recordNumbers.Length;
            var colCount = (int)Math.Floor(Math.Sqrt(imageCount));
            var rowCount = (int)Math.Ceiling(imageCount / (double)colCount);

            //---- write each image to a single sheet file ----
            var sheetWidth = colCount * sliceSize;
            var sheetHeight = rowCount * sliceSize;

            var bmpOutput = new Bitmap(sheetWidth, sheetHeight);
            using (Graphics g = Graphics.FromImage(bmpOutput))
            {
                int x = 0;
                int y = 0;

                foreach (int recordNum in recordNumbers)
                {
                    int xFrom = sliceSize * (recordNum % fromColCount);
                    int yFrom = sliceSize * (int)Math.Ceiling(recordNum / (double)fromColCount);

                    g.DrawImage(bmpInput, new Rectangle(x, y, sliceSize, sliceSize),
                            new Rectangle(xFrom, yFrom, sliceSize, sliceSize), GraphicsUnit.Pixel);

                    //---- write image for recordNumber to sheet ----
                }
            }

            var base64 = ToBase64(bmpOutput);
            bmpOutput.Dispose();
            bmpInput.Dispose();

            return base64;
        }

        public static string getStatusOfSlices(string model, string table, string image, string sizes, string baseFolder)
        {
            string statuses = "";

            var parts = sizes.Split(',');
            foreach (string part in parts)
            {
                var sliceSize = int.Parse(part);

                var sliceFolder = GetSliceFolderLocal(baseFolder, model, table, image, sliceSize);
                var count = 0;

                DirectoryInfo di = new DirectoryInfo(sliceFolder);
                string lastFn = null;
                string status = "slice folder not found";

                if (di.Exists)
                {
                    foreach (FileInfo fi in di.GetFiles("*.*"))
                    {
                        lastFn = Path.GetFileNameWithoutExtension(fi.Name);
                        count++;
                    }

                    if (count == 0)
                    {
                        status = "empty slice folder";
                    }
                    else
                    {
                        if ((count == 1) && (lastFn.StartsWith("images-")))
                        {
                            count = int.Parse(lastFn.Substring(7));     // skip over the "images-"
                            status = count + " images in sheet";
                        }
                        else
                        {
                            status = count + " files";
                        }
                    }
                }

                if (statuses.Length > 0)
                {
                    statuses += ",";
                }

                statuses += status;
            }

            return statuses;
        }

        static string ToBase64(Bitmap bmp)
        {
            var base64 = "";

            using (var ms = new MemoryStream())
            {
                bmp.Save(ms, System.Drawing.Imaging.ImageFormat.Bmp);
                var bytes = ms.ToArray();

                base64 = Convert.ToBase64String(bytes);
            }

            return base64;
        }

        public static string GetSliceFolderLocal(string baseFolder, string modelName, string tableName, string imageName, int sliceSize)
        {
            var str = baseFolder + "\\imageFiles\\" + modelName + "\\" + tableName + "\\" + imageName;

            if (sliceSize > -1)
            {
                str += "\\size" + sliceSize;
            }
            
            return str;
        }

        public static string GetSlicerFolderURL(string baseFolder, string modelName, string tableName, string imageName, int sliceSize)
        {
            var str = baseFolder + "/imageFiles/" + modelName + "/" + tableName + "/" + imageName;

            if (sliceSize > -1)
            {
                str += "/size" + sliceSize;
            }

            return str;
        }

        static void Error(string msg)
        {
            throw new Exception(msg);
        }

        static bool isValidSliceSize(int size)
        {
            int[] slices = {1, 2, 4, 8, 16, 32, 64, 128, 256, 512, 1024, 2048};

            var isValid = (Array.IndexOf(slices, size) > -1);

            return isValid;
        }
    }

}