﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace bpServer
{
    /// <summary>
    /// templateFileServer:  builds an HTML page, given a template and a data stream.
    /// Template Arguments - see batchDraw.templateFiller for more info.
    /// </summary>
    
    public static class templateFillerServer
    {
        public static string fill(string template, templateContext ctx)
        {
            //---- parse str into "{" delimited parts ----
            var newStr = "";
            var start = 0;
            var lastIndex = template.Length - 1;

            while (true)
            {
                var index = template.IndexOf("{", start);
                if ((index == -1) || (index == lastIndex))
                {
                    //---- no more entries ----
                    newStr += template.Substring(start);
                    break;
                }

                //---- copy the in-between string ----
                newStr += template.Substring(start, index-start);

                //---- we have at least 1 char following "{" ----
                var nextChar = template[index + 1];

                if (nextChar =='{')
                {
                    var index2 = template.IndexOf("}}", index + 2);
                    if (index2 == -1)
                    {
                        error("missing '}}' at end of template '{{ name }}' entry");
                    }

                    var part = template.Substring(index + 2, index2 - (index + 2));
                    part = part.Trim();
                    var value = processVar(part, ctx);

                    newStr += value;
                    start = index2 + 2;
                }
                else if (nextChar == '%')
                {
                    var index2 = template.IndexOf("%}", index + 2);
                    if (index2 == -1)
                    {
                        error("missing '%}' at end of template '{% name %}' tag");
                    }

                    var part = template.Substring(index + 2, index2 - (index + 2));
                    part = part.Trim();
                    var value = processTag(part, ctx);

                    newStr += value;
                    start = index2 + 2;
                }
                else
                {
                    newStr += template.Substring(index, 2);
                    start = index + 2;
                }
            }

            return newStr;
        }

        static string processVar(string part2, templateContext ctx)
        {
            var colName = part2.Trim();

            //---- see if a filter (or set of filters) is present ----
            var index2 = colName.IndexOf("|");
            string filter = null;

            if (index2 > -1)
            {
                filter = colName.Substring(index2 + 1).Trim();
                colName = colName.Substring(0, index2).Trim();
            }

            if (! ctx.record.ContainsKey(colName))
            {
                error("unrecognized column name: " + colName);
            }

            var value = ctx.record[colName] + "";

            //---- apply filter(s) ----
            if (filter != null)
            {
                value = applyFilter(value, filter, ctx);
            }

            if (ctx.isUnsafe)
            {
                //---- remove any potential HTML/Script from values ----
                //value = EncodeURIComponent(value);
            }

            return value;
        }

        static string processTag(string part2, templateContext ctx)
        {
            var tagName = part2.Trim();

            //---- see if a filter (or set of filters) is present ----
            var index2 = tagName.IndexOf("|");
            string filter = null;

            if (index2 > -1)
            {
                filter = tagName.Substring(index2 + 1).Trim();
                tagName = tagName.Substring(0, index2).Trim();
            }

            object value = null;

            if (tagName == "dataIndex")
            {
                value = ctx.dataIndex;
            }
            else if (tagName == "baseImagesUrl")
            {
                value = ctx.baseImagesUrl;
            }
            else if (tagName == "drawingSize")
            {
                value = ctx.drawingSize;
            }
            else if (tagName == "contentSize")
            {
                value = ctx.drawingSize - 4;
            }
            else
            {
                error("unrecognized tag name: " + tagName);
            }

            //---- apply filter(s) ----
            if (filter != null)
            {
                value = applyFilter(value, filter, ctx);
            }

            if (ctx.isUnsafe)
            {
                //---- remove any potential HTML/Script from values ----
                //value = encodeURIComponent(value);
            }

            return value.ToString();
        }

        static string applyFilter(object value, string filter, templateContext ctx)
        {
            while (filter != null)
            {
                string nextFilter = null;

                var index = filter.IndexOf("|");
                if (index > -1)
                {
                    nextFilter = filter.Substring(index + 1).Trim();
                    filter = filter.Substring(0, index).Trim();
                }

                var name = filter;
                string arg = null;

                var index3 = name.IndexOf(":");
                if (index3 > -1)
                {
                    arg = name.Substring(index3 + 1).Trim();
                    name = name.Substring(0, index3).Trim();

                    //---- remove quotes around arg, if present ----
                    if (arg.Length > 0)
                    {
                        var lastIndex = arg.Length - 1;

                        if (arg[0] == '\"' || arg[0] == '\'')
                        {
                            if (arg[lastIndex] == '\"' || arg[lastIndex] == '\'')
                            {
                                arg = arg.Substring(1, arg.Length - 2);
                            }
                        }
                    }
                }

                string[] argParts = (arg != null) ? arg.Split(',') : new string[0];

                if (name == "yesno")
                {
                    if (value is string)
                    {
                        //---- convert to boolean ----
                        value = ((string)value == "true");
                    }
                    else if (value is int)
                    {
                        value = ((int)value == 1);
                    }

                    if (! (value is bool))
                    {
                        value = argParts[2];
                    }
                    else
                    {
                        bool bvalue = (bool)value;

                        if (bvalue == true && argParts.Length >= 1)
                        {
                            value = argParts[0];
                        }
                        else if (bvalue == false && argParts.Length >= 2)
                        {
                            value = argParts[1];
                        }
                    }
                }
                else if (name == "enumtext")
                {
                    var numVal = Convert.ToInt32(value);

                    if (numVal >= 0 && numVal < argParts.Length)
                    {
                        value = argParts[numVal];
                    }
                }
                else if (name == "plus")
                {
                    double op = double.Parse(argParts[0]);
                    double dblValue = Convert.ToDouble(value);

                    dblValue = dblValue + op;
                    value = dblValue;
                }
                else if (name == "minus")
                {
                    double op = double.Parse(argParts[0]);
                    double dblValue = Convert.ToDouble(value);

                    dblValue = dblValue - op;
                    value = dblValue;
                }
                else if (name == "minPowerOfTwo")
                {
                    var dblValue = Convert.ToDouble(value);

                    dblValue = Math.Ceiling(Math.Log(dblValue, 2));     // log base 2
                    dblValue = Math.Pow(2, dblValue);

                    value = dblValue;
                }
                else if (name == "clamp")
                {
                    var dblValue = Convert.ToDouble(value);

                    if (argParts.Length != 2)
                    {
                        error("clamp filter requires 2 comma separated numeric arguments");
                    }

                    var minValue = double.Parse(argParts[0]);
                    var maxValue = double.Parse(argParts[1]);

                    if (dblValue < minValue)
                    {
                        dblValue = minValue;
                    }
                    else if (dblValue > maxValue)
                    {
                        dblValue = maxValue;
                    }

                    value = dblValue;
                }
                else
                {
                    error("unrecognized filter name: " + name);
                }

                filter = nextFilter;
            }

            return value.ToString();
        }

        static void error(string str)
        {
            throw new Exception(str);
        }

    }
}
