﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Reflection.Emit;
using System.Web;

namespace bpServer
{
    public static class StrongRows
    {
        public static int classCount;

        public static Array ConvertToStrongTypedTable(DataTable dataTable, bool useProperties)
        {
            int rowCount = dataTable.Rows.Count;

            Type newType = CreateTypeForRecord(dataTable.Columns, useProperties);
            Array arrayObj = Array.CreateInstance(newType, dataTable.Rows.Count);

            //---- build "columns" for standard access ----
            FieldInfo[] fis = newType.GetFields();

            //---- build actual records for array ----
            int rowNum = 0;
            foreach (DataRow row in dataTable.Rows)
            {
                object objRecord = Activator.CreateInstance(newType);
                arrayObj.SetValue(objRecord, rowNum);

                for (int i = 0; i < dataTable.Columns.Count; i++)
                {
                    object colValue = row[i];

                    if (colValue is DBNull)
                    {
                        colValue = null;
                    }

                    fis[i].SetValue(objRecord, colValue);
                }

                rowNum++;
            }

            return arrayObj;
        }

        static Type CreateTypeForRecord(System.Data.DataColumnCollection columns, bool useProperties)
        {
            Stopwatch sw = new Stopwatch();
            sw.Start();

            classCount++;
            string typeName = "StrongDataTable_" + (classCount);

            AssemblyName assemblyName = new AssemblyName("DynamicClasses");
            AssemblyBuilder assemblyBuilder = AppDomain.CurrentDomain.DefineDynamicAssembly(assemblyName,
                AssemblyBuilderAccess.Run);

            ModuleBuilder moduleBuilder = assemblyBuilder.DefineDynamicModule("Module");

            TypeBuilder tb = moduleBuilder.DefineType(typeName, TypeAttributes.Class |
                TypeAttributes.Public, null);

            tb.DefineDefaultConstructor(MethodAttributes.Public);

            foreach (DataColumn dc in columns)
            {
                string safeName = ConvertToDotNetSafeName(dc.ColumnName);

                Type dataType = dc.DataType;
#if xx
                if ((dc.AllowDBNull) && (!dataType.IsClass))
                {
                    //---- get type = Nullable<type> ----
                    dataType = typeof(Nullable<>).MakeGenericType(new System.Type[] { dataType });
                }
#endif
                if (useProperties)
                {

                    FieldBuilder fb = tb.DefineField("_fld_" + safeName, dataType,
                        FieldAttributes.Public);

                    PropertyBuilder pb = tb.DefineProperty(safeName,
                        System.Reflection.PropertyAttributes.None, dataType, null);

                    //---- build the property GET method ----
                    var methAttr = MethodAttributes.Public | MethodAttributes.SpecialName |
                        MethodAttributes.HideBySig;

                    MethodBuilder gmBuilder = tb.DefineMethod("get_" + safeName, methAttr, dataType,
                        Type.EmptyTypes);

                    var ilGen = gmBuilder.GetILGenerator();
                    ilGen.Emit(OpCodes.Ldarg_0);            // "this"
                    ilGen.Emit(OpCodes.Ldfld, fb);          // field builder object
                    ilGen.Emit(OpCodes.Ret);

                    //---- build the property SET method ----
                    MethodBuilder smBuilder = tb.DefineMethod("set_" + safeName, methAttr, null,
                        new Type[] { dataType });

                    ilGen = smBuilder.GetILGenerator();
                    ilGen.Emit(OpCodes.Ldarg_0);            // "this"
                    ilGen.Emit(OpCodes.Ldarg_1);            // value
                    ilGen.Emit(OpCodes.Stfld, fb);          // field builder object
                    ilGen.Emit(OpCodes.Ret);

                    //---- associate get/set methods with the property ----
                    pb.SetGetMethod(gmBuilder);
                    pb.SetSetMethod(smBuilder);
                }
                else
                {
                    FieldBuilder fb = tb.DefineField(safeName, dataType,
                        FieldAttributes.Public);
                }

            }

            //GenerateEquals(tb, fields);
            //GenerateGetHashCode(tb, fields);

            Type type = tb.CreateType();

            sw.Stop();

            return type;
        }

        public static string ConvertToDotNetSafeName(string name)
        {
            string safeName = name;

            safeName = safeName.Replace(" ", "_");
            safeName = safeName.Replace("-", "_");
            safeName = safeName.Replace("#", "_");

            //safeName = ExpScanner.AdjustCSharpKeywords(safeName);

            return safeName;
        }
    }
}