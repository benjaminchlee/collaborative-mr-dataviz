﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace bpServer
{
    public class SessionData
    {
        public string SessionName;              // name of session (basis for sharing data)
        public string Data;                     // actual data (json format)
        public int SourceSubscriber;            // last subscriber that changed data
        public string SourceTimeStamp;          // when last change occured
        public string SourceViewName;           // view name of SourceSubscriber that made change
        public int DataChangeNumber;            // the data revision number
        public List<int> Subscribers;           // list of subscribers to this session
        public int SubscribersChangeNumber;     // revision number for subscribers list
        public Int64 UniqueCmdNumber;
        public Queue<Dictionary<string, object>> sharedCmds;

        public SessionData(string sessionName)
        {
            this.SessionName = sessionName;
            Subscribers = new List<int>();

            sharedCmds = new Queue<Dictionary<string, object>>();
        }

    }

    public class SubscriberData
    {
        public string IPAddress;            // unique IP address of machine making request
        public string processId;            // process id of app on machine

        public string ComputerName;         // friendly name of machine
        public string UserName;             // login name of user running app
        public string ProcessName;          // friendly name of app

        public int SubscriberId;            // assigned by server

        public SubscriberData(string ipAddress, string instanceId, string computerName, string userName, string processName,
          int subscriberId)
        {
            this.IPAddress = ipAddress;
            this.processId = instanceId;
            this.ComputerName = computerName;
            this.UserName = userName;
            this.ProcessName = processName;
            this.SubscriberId = subscriberId;
        }
    }

    public class SessionRequest
    {
        public string sessionName;
        public int lastChangeNum;
    }

    public class SessionResult
    {
        public string sessionName;
        public int lastChangeNum;
        public string data;

        //---- who did this change ----
        public string viewName;
        public string appName;
        public string computerName;
        public string userName;
        public string changedTime;
    }

}