﻿using System;
using System.Net;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using System.Reflection.Emit;
using System.Text;
using System.Data;

namespace bpServer
{
    /// <summary>
    /// This class is used to get data from SQL and Excel files, thru OleDb.  All requests
    /// are handles synchronously, so they should NOT be called on the 
    /// foreground thread of a client app.  
    /// </summary>
    public class CsvLight 
    {
        const int inputBufferSize = 64*1024;         
        const char CR = '\r';
        const char LF = '\n';
        const char QUOTE = '\"';

        //---- data for properties ----
        protected char delimeter;
        protected char typeDelimiter;
        protected bool hasHeader;
        protected bool inferTypes;

        protected StreamReader sr;
        protected char EOF;
        protected char ch;
        //protected string[] parts = new string[]();
        protected string[] parts;
        protected int lineNum;

        //---- input buffering (in favor of streamed char processing) ----
        protected char[] inputBuffer;
        protected int inputIndex;
        protected int inputLen;         // keep in variable for faster reference
        protected string partialField;  // when field we are building is split across buffers

        public bool InferTypes { get { return inferTypes; } set { SetInferTypes(value); } }
        public char Delimeter { get { return delimeter; } set { SetDelimeter(value); } }
        public char TypeDelimeter { get { return typeDelimiter; } set { SetTypeDelimeter(value); } }
        public bool HasHeader { get { return hasHeader; } set { SetHasHeader(value); } }

        public CsvLight()
        {
            delimeter = ',';
            hasHeader = true;
            inferTypes = false;
            typeDelimiter = (char)0;

            unchecked
            {
                EOF = (char)-1;
            }

            inputBuffer = new char[inputBufferSize];
            inputLen = 0;
        }

        void FillBuffer(int start)
        {
            if (start > -1)
            {
                //---- a field we are processing is split across input buffers, so store what ----
                //---- we have seen so far in "partialField" ----
                partialField += new string(inputBuffer, start, inputLen-start);
            }

            if (sr.EndOfStream)
            {
                inputBuffer[0] = EOF;
                inputLen = 1;
            }
            else
            {
                inputLen = sr.Read(inputBuffer, 0, inputBufferSize);
            }
            inputIndex = 0;
        }

        void SetInferTypes(bool value)
        {
            inferTypes = value;
        }

        void SetDelimeter(char value)
        {
            delimeter = value;
        }

        void SetTypeDelimeter(char value)
        {
            typeDelimiter = value;
        }

        void SetHasHeader(bool value)
        {
            hasHeader = value;
        }

        void BuildColumns(DataTable table)
        {
            table.Columns.Clear();

            for (int i = 0; i < parts.Length; i++)
            {
                if (parts[i] == null)       // ran out of data on this line
                {
                    break;
                }

                string name = parts[i].Trim();
                Type dataType = typeof(string);

                if (!hasHeader)
                {
                    name = "Col" + (i + 1);
                }
                else
                {
                    if ((name == null) || (name == ""))
                    {
                        break;      // end of valid columns
                    }

                    if (typeDelimiter != (char)(0))
                    {
                        string[] px = name.Split(typeDelimiter);
                        name = px[0];

                        if (px.Length > 1)
                        {
                            string typeName = px[1];
                            dataType = LookUpType(typeName);
                        }
                    }

                    name = ConvertToDotNetSafeName(name);
                }

                DataColumn col = new DataColumn(name, dataType);
                table.Columns.Add(col);
            }
        }

        Type LookUpType(string name)
        {
            Type type = typeof(string);

            if ((name != null) && (name != ""))     // valid string
            {
                name = name.ToLower();

                if (name == "number")
                {
                    type = typeof(double);
                }
                else if (name == "date")
                {
                    type = typeof(DateTime);
                }
                else if (name == "bool")
                {
                    type = typeof(bool);
                }
                else if (name == "string")
                {
                    type = typeof(string);
                }
                else
                {
                    throw new Exception("Unrecognized type in CSV header: " + name);
                }
            }

            return type;
        }

        void InferColumnTypesFromData(DataTable table)
        {
            List<InferType> inferTypes = new List<InferType>();

            while (parts != null)
            {
                //---- ensure "inferTypes" list is big enough for current row ----
                while (inferTypes.Count < table.Columns.Count)
                {
                    inferTypes.Add(InferType.Any);
                }

                int colCount = Math.Min(table.Columns.Count, parts.Length);

                for (int i = 0; i < colCount; i++)
                {
                    InferType result = InferDataType(parts[i], inferTypes[i]);
                    if (result != inferTypes[i])
                    {
                        if (inferTypes[i] != InferType.Any)
                        {
                            Debug.WriteLine("Field changed type on line: " + lineNum);
                        }

                        inferTypes[i] = result;
                    }
                }

                BuildLine();
            }

            //---- ensure columns in large enough ----
            int index = table.Columns.Count;

            while (table.Columns.Count < inferTypes.Count)
            {
                string colName = "Col" + (index + 1);
                table.Columns.Add(new DataColumn(colName, typeof(string)));
                index++;
            }

            //---- update the column types ----
            for (int i = 0; i < inferTypes.Count; i++)
            {
                Type[] inferTypeTypes = { typeof(string), typeof(string), 
                                            typeof(double), typeof(DateTime), typeof(bool) };

                InferType inferType = inferTypes[i];
                table.Columns[i].DataType = inferTypeTypes[(int)inferType];
            }
        }

        public static InferType InferDataType(string value, InferType inferType)
        {
            if (inferType != InferType.String)
            {
                if (value == "")
                {
                    // If no type has yet been assigned, assume it's a string 
                    // [otherwise we'll assume it's just a missing value of its established type]
                    if (inferType == InferType.Any) 
                    {
                        inferType = InferType.String;
                    }
                }
                else
                {
                    //---- inferType has not yet been firmly fixed ----
                    double dbl = 0;
                    if (double.TryParse(value, out dbl))
                    {
                        //---- parsed: DOUBLE ----
                        if (inferType != InferType.Number)
                        {
                            if (inferType == InferType.Any)
                            {
                                inferType = InferType.Number;
                            }
                            else if (inferType == InferType.Bool)
                            {
                                //---- mixed types - make it a string ----
                                inferType = InferType.String;
                            }
                        }
                    }
                    else
                    {
                        DateTime dt;
                        if (DateTime.TryParse(value, out dt))
                        {
                            //---- parsed: DATE ----
                            if (inferType != InferType.Date)
                            {
                                if ((inferType == InferType.Any) || (inferType == InferType.Date))
                                {
                                    inferType = InferType.Date;
                                }
                                //---- mixed types - make it a string ----
                                else if (inferType == InferType.Bool)
                                {
                                    inferType = InferType.String;
                                }
                            }
                        }
                        else
                        {
                            value = value.ToLower();
                            if ((value == "true") || (value == "false"))
                            {
                                //---- parsed: BOOLEAN ----
                                if (inferType != InferType.Bool)
                                {
                                    if (inferType == InferType.Any)
                                    {
                                        inferType = InferType.Bool;
                                    }
                                    else
                                    {
                                        //---- mixed types - make it a string ----
                                        inferType = InferType.String;
                                    }
                                }
                            }
                            else
                            {
                                //---- non-empty string that doesn't parse Double/Date/Bool - must be string ----
                                inferType = InferType.String;
                            }
                        }
                    }
                }
            }

            return inferType;
        }

        private char[] _trimmableChars = new char[] { ' ', '\"' };

        /// <summary>
        /// The next char is already stored in "ch".  This is the critical loop 
        /// for perf, so inlining for ReadNextChar() occurs often (7 times).
        /// </summary>
        int BuildLineCore()
        {
            //parts.Clear();
            int start = -1;
            int end = 0;
            bool embeddedQuotes = false;
            int fieldNum = 0;

            lineNum++;

            while (ch != EOF)
            {
                //---- process next field ----
                partialField = "";
                start = -1;             // no valid char(s) in buffer 

                //---- trim leading spaces early so we find starting quote correctly ----
                while (ch == ' ')
                {
                    //ch = ReadNextChar(ref start);
                    if (inputIndex == inputLen)
                    {
                        FillBuffer(start);
                        start = -1;             // no valid char(s) in buffer
                    }
                    ch = inputBuffer[inputIndex++];
                }

                start = inputIndex - 1;

                /*
                // Commented out by RichardH 1/27/14: This code is buggy when the value is like {"Foo "Bar""}
                // so we now handle QUOTE characters using _trimmableChars after reading the entire value
                if (ch == QUOTE)         // quoted field has special handling
                {
                    //ch = ReadNextChar(ref start);
                    if (inputIndex == inputLen)
                    {
                        //---- do not copy quote into partial buffer ----
                        FillBuffer(-1);     // start);
                        start = 0;
                    }
                    ch = inputBuffer[inputIndex++];
                    start = inputIndex - 1;             // since we skipped over the leading QUOTE
                    
                    while (true) 
                    {
                        if (ch == EOF)
                        {
                            throw new Exception("EOF encountered while looking for end of quoted field");
                        }

                        if (ch == QUOTE)
                        {
                            //---- look at following char to see if this is a QUOTE PAIR ----

                            //ch = ReadNextChar(ref start);
                            if (inputIndex == inputLen)
                            {
                                //---- do not copy quote into partial buffer ----
                                FillBuffer(-1);     // start);
                                start = 0;
                            }
                            ch = inputBuffer[inputIndex++];

                            if (ch != QUOTE)
                            {
                                if (inputIndex == 1)        // need to remove quote from last buffer
                                {
                                    //---- remove QUOTE from end of partialField ----
                                    if (partialField.Length > 0)
                                    {
                                        partialField = partialField.Substring(0, partialField.Length - 1);
                                        end = 0;
                                    }
                                }
                                else
                                {
                                    end = inputIndex - 2;
                                }
                                break;
                            }
                            else
                            {
                                embeddedQuotes = true;
                            }

                            //---- QUOTE PAIR just falls thru as a single QUOTE ----
                        }

                        //ch = ReadNextChar(ref start);
                        if (inputIndex == inputLen)
                        {
                            FillBuffer(start);
                            start = 0;
                        }
                        ch = inputBuffer[inputIndex++];
                    }
                }
                */

                char prevCh = '\0';

                start = inputIndex-1;
                while ((ch != CR) && (ch != LF) && (ch != EOF) && (ch != delimeter))
                {
                    //ch = ReadNextChar(ref start);
                    if (inputIndex == inputLen)
                    {
                        FillBuffer(start);
                        start = 0;
                    }
                    prevCh = ch;
                    ch = inputBuffer[inputIndex++];

                    if ((ch == prevCh) && (ch == '\"'))
                    {
                        embeddedQuotes = true;
                    }
                }

                end = inputIndex - 1;

                //---- finish field ----
                if (fieldNum < parts.Length)
                {
                    string part = new string(inputBuffer, start, end - start);
                    if (partialField != "")
                    {
                        part = partialField + part;
                    }

                    // Remove leading/trailing space and double-quote characters
                    part = part.Trim(_trimmableChars);
                    
                    if (embeddedQuotes)
                    {
                        part = part.Replace("\"\"", "\"");
                    }

                    //parts.Add(part);
                    parts[fieldNum++] = part;
                }

                //---- skip over CR/LF if present ----
                if (ch == CR)
                {
                    //ch = ReadNextChar();
                    if (inputIndex == inputLen)
                    {
                        FillBuffer(-1);
                        start = 0;
                    }
                    ch = inputBuffer[inputIndex++];

                }

                if (ch == LF)
                {
                    //ch = ReadNextChar();
                    if (inputIndex == inputLen)
                    {
                        FillBuffer(-1);
                        start = 0;
                    }
                    ch = inputBuffer[inputIndex++];

                    break;
                }

                if (ch == delimeter)
                {
                    //ch = ReadNextChar();
                    if (inputIndex == inputLen)
                    {
                        FillBuffer(-1);
                        start = 0;
                    }
                    ch = inputBuffer[inputIndex++];
                }
            }

            //DumpLine(parts);

            if (fieldNum == 0)      // EOF
            {
                parts = null;
            }

            return fieldNum;
        }

        int BuildLine()
        {
            int valueCount = BuildLineCore();
            while ((valueCount > 0) && (valueCount < parts.Length))
            {
                Debug.WriteLine("Skipping bad CSV line: " + lineNum);

                valueCount = BuildLineCore();
            }

            return valueCount;
        }

        Char ReadNextChar()
        {
            //---- NEXT CHAR ----
            if (inputIndex == inputLen)
            {
                FillBuffer(-1);
            }

            ch = inputBuffer[inputIndex++];
            return ch;
        }

        Char ReadNextChar(ref int start)
        {
            //---- NEXT CHAR ----
            if (inputIndex == inputLen)
            {
                FillBuffer(start);
                start = 0;
            }

            ch = inputBuffer[inputIndex++];
            return ch;
        }

        void DumpLine(string[] parts)
        {
            string str = "Line: ";

            foreach (string part in parts)
            {
                str += part + "^";
            }

            Debug.WriteLine(str);
        }

        string[] BuildFirstLine()
        {
            //----- holds the fields for the header names (and later the data fields in a record) ----
            parts = new string[4096];
            lineNum = 0;

            ReadNextChar();

            int partCount = BuildLineCore();

            if (true)       // hasHeader)
            {
                string[] newParts = new string[partCount];
                Array.Copy(parts, newParts, partCount);

                parts = newParts;           // exactly the number of fields in the header
            }
            else
            {
                //BuildLine();
            }

            return parts;
        }

        public DataTable GetDataFromString(string text)
        {
            DataTable table = null;

            byte[] buffer = System.Text.UTF8Encoding.UTF8.GetBytes(text);
            table = GetData(buffer);

            return table;
        }

        public DataTable GetData(byte[] csvBytes)
        {
            DataTable table = new DataTable();

            if (inferTypes)
            {
                //---- first pass - infer column types ----
                using (MemoryStream ms = new MemoryStream(csvBytes))
                {

                    using (sr = new StreamReader(ms))
                    {
                        BuildFirstLine();

                        BuildColumns(table);

                        if (hasHeader)
                        {
                            BuildLine();
                        }

                        InferColumnTypesFromData(table);
                    }
                }
            }

            //---- second pass - get selected data ----
            using (MemoryStream ms2 = new MemoryStream(csvBytes))
            {
                using (sr = new StreamReader(ms2))
                {
                    BuildFirstLine();

                    if (table.Columns.Count == 0)
                    {
                        BuildColumns(table);
                    }

                    if (hasHeader)
                    {
                        BuildLine();
                    }

                    while (parts != null)
                    {
                        DataRow row = table.NewRow();
                        //row[0] = " hi there";

                        AddDataToRow(row, parts);

                        table.Rows.Add(row);

                        BuildLine();
                    }
                }
            }

            return table;
        }

#if WPF
        public DataTable GetDataFromFile(string fn)
        {
            DataTable table = new DataTable();

            try
            {
                if (File.Exists(fn))
                {
                    using(Stream stream = new FileStream(fn, FileMode.Open))
                    {
                        table = GetData(stream);
                    }
                }
                else
                {
                    throw new Exception("Cannot access file: " + fn);
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine("CsvLight.GetRows Exception: " + ex.Message);
            }

            return table;
        }
#endif

        int FindColumn(List<DataColumn> cols, string name)
        {
            int index = -1;

            for (int i = 0; i < cols.Count; i++)
            {
                if (String.Compare(cols[i].ColumnName, name, true) == 0)
                {
                    index = i;
                    break;
                }
            }

            return index;
        }

        void AddDataToRow(DataRow row, string[] strValues)
        {
            int colCount = row.Table.Columns.Count;

            for (int colNum=0; colNum < colCount; colNum++)
            {
                if (colNum >= strValues.Length)
                {
                    break;
                }

                string value = strValues[colNum];
                Type dataType = row.Table.Columns[colNum].DataType;

                if ((value.Length > 0) && (value[value.Length-1] == ' '))
                {
                    value = value.TrimEnd();
                }

                if (dataType == typeof(string))
                {
                    row[colNum] = value;
                }
                else if (dataType == typeof(double))
                {
                    if (value == "")
                    {
                        row[colNum] = 0;
                    }
                    else
                    {
                        row[colNum] = Convert.ToDouble(value);
                    }
                }
                else if (dataType == typeof(DateTime))
                {
                    if (value != null && value != "")
                    {
                        row[colNum] = Convert.ToDateTime(value);
                    }
                }
                else
                {
                    row[colNum] = Convert.ToBoolean(value);
                }
            }

        }

        public string ConvertToDotNetSafeName(string name)
        {
            string safeName = name;

            safeName = safeName.Replace(" ", "_");
            safeName = safeName.Replace("-", "_");
            safeName = safeName.Replace("#", "_");

            return safeName;
        }
    }

    public class FieldExp
    {
        public string colName;
        public string asName;
        public string funcName;
        public int sourceColNum;
        public string colType;

        public override string ToString()
        {
            return colName;
        }
    }

    public enum InferType
    {
        Any,
        String,
        Number,
        Date,
        Bool,
    }

}
