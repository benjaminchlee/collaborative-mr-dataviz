﻿using System;
using System.Speech.Recognition;
using System.Threading;

namespace bpServer
{
    public class SpeechRecog
    {

        // Indicate whether the asynchronous emulate recognition
        // operation has completed.
        static bool completed;

        static private GrammarBuilder CreateChartGrammar()
        {

            Choices scatterChoices = new Choices(new string[] { "scatter", "scatterplot", "xy", "xyplot", "map" });
            GrammarBuilder scatterElem = new GrammarBuilder(scatterChoices);

            Choices colChoices = new Choices(new string[] { "column", "histogram" });
            GrammarBuilder colElem = new GrammarBuilder(colChoices);

            Choices tdChoices = new Choices(new string[] { "3d", "scatter3d" });
            GrammarBuilder tdElem = new GrammarBuilder(tdChoices);

            Choices typeChoices = new Choices(new GrammarBuilder[] { scatterChoices, colChoices, tdChoices });
            GrammarBuilder typeElem = new GrammarBuilder(typeChoices);

            Choices chartChoices = new Choices(new string[] { "chart", "view", "plot", "layout" });
            GrammarBuilder chartElem = new GrammarBuilder(chartChoices);
            chartElem.Append(typeElem);

            return chartElem;
        }

        static private GrammarBuilder CreateColorGrammar()
        {
            // Create a set of color choices.
            Choices colorChoice = new Choices(new string[] { "red", "green", "blue" });
            GrammarBuilder colorElement = new GrammarBuilder(colorChoice);

            // Create grammar builders for the two versions of the phrase.
            GrammarBuilder makePhrase = new GrammarBuilder("set background to");
            makePhrase.Append(colorElement);

            GrammarBuilder setPhrase = new GrammarBuilder("Set color to");
            setPhrase.Append(colorElement);

            // Create a Choices for the two alternative phrases, convert the Choices
            // to a GrammarBuilder, and construct the grammar from the result.
            Choices bothChoices = new Choices(new GrammarBuilder[] { makePhrase, setPhrase });
            GrammarBuilder gb = (GrammarBuilder)bothChoices;
            return gb;
        }

        static void Main(string[] args)
        {

            // Initialize an instance of the shared recognizer.
            using (SpeechRecognitionEngine recognizer = new SpeechRecognitionEngine())
            {

                // Create and load a sample grammar.
                GrammarBuilder colorGrammer = CreateColorGrammar();
                GrammarBuilder chartGrammer = CreateChartGrammar();

                Choices testChoices = new Choices(new GrammarBuilder[] { colorGrammer, chartGrammer });
                Grammar testGrammar = new Grammar((GrammarBuilder)testChoices);

                recognizer.LoadGrammar(testGrammar);

                // Attach event handlers for recognition events.
                recognizer.SpeechRecognized +=
                  new EventHandler<SpeechRecognizedEventArgs>(
                    SpeechRecognizedHandler);

                recognizer.EmulateRecognizeCompleted +=
                  new EventHandler<EmulateRecognizeCompletedEventArgs>(
                    EmulateRecognizeCompletedHandler);

                //recognizer.SetInputToAudioStream();

                completed = false;

                // Start asynchronous emulated recognition. 
                // This matches the grammar and generates a SpeechRecognized event.
                //recognizer.EmulateRecognizeAsync("testing testing");

                // Wait for the asynchronous operation to complete.
                while (!completed)
                {
                    Thread.Sleep(333);
                }

                completed = false;

                // Start asynchronous emulated recognition.
                // This does not match the grammar or generate a SpeechRecognized event.
                recognizer.EmulateRecognizeAsync("testing one two three");

                // Wait for the asynchronous operation to complete.
                while (!completed)
                {
                    Thread.Sleep(333);
                }
            }

            Console.WriteLine();
            Console.WriteLine("Press any key to exit...");
            Console.ReadKey();
        }

        // Handle the SpeechRecognized event.
        static void SpeechRecognizedHandler(
          object sender, SpeechRecognizedEventArgs e)
        {
            if (e.Result != null)
            {
                Console.WriteLine("Recognition result = {0}",
                  e.Result.Text ?? "<no text>");
            }
            else
            {
                Console.WriteLine("No recognition result");
            }
        }

        // Handle the SpeechRecognizeCompleted event.
        static void EmulateRecognizeCompletedHandler(
          object sender, EmulateRecognizeCompletedEventArgs e)
        {
            if (e.Result == null)
            {
                Console.WriteLine("No result generated.");
            }

            // Indicate the asynchronous operation is complete.
            completed = true;
        }
    }
}
