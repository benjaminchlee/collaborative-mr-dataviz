/* DROP DATABASE [BeachParty] */

CREATE DATABASE [BeachParty];

use BeachParty;  CREATE TABLE [ACTIONS] 
(
	[Id] [Int] IDENTITY,
	[Timestamp] [Datetime] NOT NULL,
	[Session] [varchar](50) NOT NULL,
	[Gesture] [varchar](50) NOT NULL,
	[ElementId] [varchar](50) NOT NULL,
	[ElementType] [varchar](50) NOT NULL,
	[Action] [varchar](50) NOT NULL,
	[Target] [varchar](50),
	[Name1] [varchar](50),
	[Value1] [varchar](50),
	[Name2] [varchar](50),
	[Value2] [varchar](50),
	[Name3] [varchar](50),
	[Value3] [varchar](50),
	[Name4] [varchar] (50),
	[Value4] [varchar](50),
);

CREATE LOGIN vibedemo WITH PASSWORD = 'vibedemo';
use BeachParty; Create User vibedemo from Login vibedemo; GRANT ALL ON dbo.Actions TO vibedemo;

use BeachParty; select top 10 * from Actions order by Id desc;

/* use BeachParty; delete from actions; */
