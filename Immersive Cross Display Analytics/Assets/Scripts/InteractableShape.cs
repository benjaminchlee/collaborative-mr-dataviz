﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VRTK;

public class InteractableShape : MonoBehaviour {

    public int Index { get; private set; }
    public bool IsSelected { get; private set; }

    // Use this for initialization
    void Start () {
        IsSelected = false;
    }

    public void SetIndex(int index)
    {
        Index = index;
    }
        
    /// <summary>
    /// Selects the shape. This function also calls upon the appropriate methods which send its selected status
    /// to SandDance.
    /// </summary>
    public void ShapeSelected()
    {
        SetShapeSelected();

        ScreenManager.Instance.ShapeSelected(Index);
    }

    /// <summary>
    /// Sets the shape as selected. This function is meant to be used to batch select shapes and therefore assumes
    /// that all management of the selected indices have been performed by other scripts.
    /// </summary>
    public void SetShapeSelected()
    {
        IsSelected = true;
    }

    public void ShapeDeselected()
    {
        SetShapeDeselected();

        ScreenManager.Instance.ShapeDeselected(Index);
    }

    public void SetShapeDeselected()
    {
        IsSelected = false;
    }
}
