﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using WebSocketSharp;
using WebSocketSharp.Server;
using VRTK;
using System;

public class WebSocketManager : MonoBehaviour
{

    public static WebSocketManager Instance { get; private set; }

    [Tooltip("The port that the websocket will use.")]
    private int port = 81;

    private WebSocketServer wssv;
    private WebSocketSessionManager wssm;

    public event EventHandler<ChartDetailsEventArgs> GetChartDetailsCompleted;
    public event EventHandler<ShapesDetailsEventArgs> GetShapesDetailsCompleted;
    public event EventHandler<ElementPositionsEventArgs> GetHtmlElementsCompleted;
    public event EventHandler<SelectedIndicesEventArgs> GetSelectedIndicesCompleted;

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
    }

    // Use this for initialization
    private void Start()
    {
        InitialiseServer();
    }

    public void InitialiseServer()
    {
        wssv = new WebSocketServer(port);
        wssv.AddWebSocketService<SandDanceSocket>("/");
        wssm = wssv.WebSocketServices["/"].Sessions;

        wssv.Start();

        Debug.Log("Websocket started on port " + port);
    }

    public void OnConnect()
    {
        Debug.Log("Session connected");
    }

    public void MessageReceived(string text)
    {
        Debug.Log(text);
        string command = text.Split('@')[0];
        string data = text.Split('@')[1];

        if (command.Equals("GetChartDetails"))
        {
            // Deserialise JSON string
            ChartDetails chartDetails = JsonUtility.FromJson<ChartDetails>(data);

            ChartDetailsEventArgs e = new ChartDetailsEventArgs();

            e.X = SceneManager.Instance.dataObject.dimensionToIndex(chartDetails.x) != -1 ? chartDetails.x : null;
            e.Y = SceneManager.Instance.dataObject.dimensionToIndex(chartDetails.y) != -1 ? chartDetails.y : null;
            e.Z = SceneManager.Instance.dataObject.dimensionToIndex(chartDetails.z) != -1 ? chartDetails.z : null;
            
            int r = 0;
            int g = 0;
            int b = 0;
            string colorString = chartDetails.color.Remove(1);  // Remove # symbol
            // If hexadecimal string is 3 characters, each character duplicated twice in a row represents an r, g or b value
            if (colorString.Length == 3)
            {
                r = int.Parse(colorString.Substring(0, 1) + colorString.Substring(0, 1), System.Globalization.NumberStyles.AllowHexSpecifier);
                g = int.Parse(colorString.Substring(1, 1) + colorString.Substring(1, 1), System.Globalization.NumberStyles.AllowHexSpecifier);
                b = int.Parse(colorString.Substring(2, 1) + colorString.Substring(2, 1), System.Globalization.NumberStyles.AllowHexSpecifier);
            }
            // If hexadecimal string is 6 characters, each set of 2 characters represents an r, g or b value
            else if (colorString.Length == 6)
            {
                r = int.Parse(colorString.Substring(0, 2), System.Globalization.NumberStyles.AllowHexSpecifier);
                g = int.Parse(colorString.Substring(2, 2), System.Globalization.NumberStyles.AllowHexSpecifier);
                b = int.Parse(colorString.Substring(4, 2), System.Globalization.NumberStyles.AllowHexSpecifier);
            }
            e.Color = new Color(r, g, b);
            e.ColorBy = SceneManager.Instance.dataObject.dimensionToIndex(chartDetails.colorBy) != -1 ? chartDetails.colorBy : null;

            if (chartDetails.colorPalette != null)
            {
                List<Color> palette = new List<Color>();
                foreach (string rgbString in chartDetails.colorPalette)
                {
                    // Remove leading RGB( and trailing ), and split by , to attain the R, G and B values
                    string[] rgbValues = rgbString.Replace("rgb(", "").Replace(")", "").Split(',');
                    int[] rgb = Array.ConvertAll(rgbValues, x => int.Parse(x));

                    palette.Add(new Color(rgb[0], rgb[1], rgb[2]));
                }
                e.ColorPalette = palette.ToArray();
            }

            e.ChartType = chartDetails.chartType;

            OnGetChartDetailsCompleted(e);
        }
        else if (command.Equals("GetShapesDetails"))
        {
            string[] tmp = data.Split('$');
            string shapeData = tmp[0];
            string rangeData = tmp[1];

            ShapeDetails[] shapeDetails = JsonHelper.GetJsonArray<ShapeDetails>(shapeData);
            // Sort the primary keys such that the order which they appear in the array is the original index
            shapeDetails = shapeDetails.OrderBy(x => int.Parse(x.primaryKey)).ToArray();
            ShapeRanges shapeRanges = JsonUtility.FromJson<ShapeRanges>(rangeData);

            ShapesDetailsEventArgs e = new ShapesDetailsEventArgs
            {
                Details = shapeDetails,
                Ranges = shapeRanges
            };

            OnGetShapesDetailsCompleted(e);
        }
        else if (command.Equals("GetHtmlElements"))
        {
            HtmlElement[] elements = JsonHelper.GetJsonArray<HtmlElement>(data);

            HtmlElement window = elements.FirstOrDefault(x => x.id == "window");
            HtmlElement chart = elements.FirstOrDefault(x => x.id == "chartUx");

            ElementPositionsEventArgs e = new ElementPositionsEventArgs
            {
                Window = window,
                Chart = chart
            };

            List<HtmlElement> tmp = new List<HtmlElement>(elements);
            tmp.Remove(window);
            tmp.Remove(chart);
            e.Positions = tmp.ToArray();

            OnGetHtmlElementsCompleted(e);
        }
        else if (command.Equals("GetSelectedIndices"))
        {
            string[] values;

            if (data != "")
                values = data.Split(',');
            else
                values = new string[0];

            int[] indices = values.Select(x => int.Parse(x)).ToArray();

            SelectedIndicesEventArgs e = new SelectedIndicesEventArgs
            {
                SelectedIndices = indices
            };

            OnGetSelectedIndicesCompleted(e);
        }
    }

    public void GetChartDetails()
    {
        wssm.Broadcast("GetChartDetails");
    }

    public void GetShapesDetails()
    {
        wssm.Broadcast("GetShapesDetails");
    }

    public void GetElementPositions()
    {
        wssm.Broadcast("GetElementPositions");
    }

    public void SetSelectedIndices(int[] selectedIndices)
    {
        wssm.Broadcast("SetSelectedIndices;" + string.Join(",", selectedIndices));
    }

    public void HtmlElementClicked(HtmlElement el)
    {
        string data = JsonUtility.ToJson(el);
        wssm.Broadcast("HtmlElementClicked;" + data);
    }

    public void CloseHtmlPopupMenus()
    {
        wssm.Broadcast("CloseHtmlPopupMenus");
    }

    public void SetDimension(string name, Dimension dimension)
    {
        string d = dimension.ToString().ToLower();
        wssm.Broadcast("SetDimension;" + name + "|" + d);
    }

    public void SetVisualisationAsColumn(string x)
    {
        wssm.Broadcast("SetColumnVisualisation;" + x);
    }

    public void SetVisualisationAsScatter(string x, string y)
    {
        wssm.Broadcast("SetScatterVisualisation;" + x + "|" + y);
    }

    public void SetVisualisationAsColouredScatter(string x, string y, string z)
    {
        wssm.Broadcast("SetColouredScatterVisualisation;" + x + "|" + y + "|" + z);
    }

    public void SetFilteredIndices(int[] filtered)
    {
        wssm.Broadcast("SetFilteredIndices;" + string.Join(",", filtered));
    }

    protected virtual void OnGetChartDetailsCompleted(ChartDetailsEventArgs e)
    {
        GetChartDetailsCompleted?.Invoke(this, e);
    }

    protected virtual void OnGetShapesDetailsCompleted(ShapesDetailsEventArgs e)
    {
        GetShapesDetailsCompleted?.Invoke(this, e);
    }

    protected virtual void OnGetHtmlElementsCompleted(ElementPositionsEventArgs e)
    {
        GetHtmlElementsCompleted?.Invoke(this, e);
    }

    protected virtual void OnGetSelectedIndicesCompleted(SelectedIndicesEventArgs e)
    {
        GetSelectedIndicesCompleted?.Invoke(this, e);
    }

    [Serializable]
    private class ChartDetails
    {
        public string x;
        public string y;
        public string z;
        public int[] selectedIndices;
        public string color;
        public string colorBy;
        public string[] colorPalette;
        public int chartType;
    }
}

public class SandDanceSocket : WebSocketBehavior
{
    protected override void OnOpen()
    {
        WebSocketManager.Instance.OnConnect();
    }

    protected override void OnMessage(MessageEventArgs e)
    {
        WebSocketManager.Instance.MessageReceived(e.Data.ToString());
    }
}

[Serializable]
public class ShapeDetails
{
    public float blueChannel;
    public float colorIndex;
    public float depth;
    public float greenChannel;
    public float imageIndex;
    public float height;
    public float opacity;
    public string primaryKey;
    public float redChannel;
    public float staggerOffset;
    public float theta;
    public float width;
    public float x;
    public float y;
    public float z;
}

[Serializable]
public class HtmlElement
{
    public string id;
    public string tagName;
    public string className;
    public string title;
    public float left;
    public float top;
    public float width;
    public float height;
}

[Serializable]
public class ShapeRanges
{
    public float xMin;
    public float xMax;
    public float yMin;
    public float yMax;
}

public class ChartDetailsEventArgs : EventArgs
{
    public string X { get; set; }
    public string Y { get; set; }
    public string Z { get; set; }
    public Color Color { get; set; }
    public string ColorBy { get; set; }
    public Color[] ColorPalette { get; set; }
    public int ChartType { get; set; }
}

public class ShapesDetailsEventArgs : EventArgs
{
    public ShapeDetails[] Details;
    public ShapeRanges Ranges;
}

public class ElementPositionsEventArgs : EventArgs
{
    public HtmlElement Window;
    public HtmlElement Chart;
    public HtmlElement[] Positions;
}

public class SelectedIndicesEventArgs : EventArgs
{
    public int[] SelectedIndices;
}

public class JsonHelper
{
    public static T[] GetJsonArray<T>(string json)
    {
        string newJson = "{ \"array\": " + json + "}";
        Wrapper<T> wrapper = JsonUtility.FromJson<Wrapper<T>>(newJson);
        return wrapper.array;
    }

    public static string ArrayToJson<T>(T[] array)
    {
        Wrapper<T> wrapper = new Wrapper<T>();
        wrapper.array = array;
        return JsonUtility.ToJson(wrapper);
    }

    [Serializable]
    private class Wrapper<T>
    {
        public T[] array;
    }
}