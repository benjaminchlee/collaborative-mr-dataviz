﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using VRTK;

public class TestManager : MonoBehaviour {

    [SerializeField] [Tooltip("Determines if the users positions is to be logged.")]
    private bool isLogging = false;
    [SerializeField] [Tooltip("The output file directory to save log files to.")]
    private string path = "Assets/Logs";
    [SerializeField] [Tooltip("The time interval in seconds between logs.")]
    private float logInterval;
    [SerializeField] [Tooltip("The name of the current participant.")]
    private string participantName = "";
    [SerializeField] [Tooltip("The current task being performed that gets logged to the output.")]
    private string currentTask = "";

    private StreamWriter writer;
    private Camera camera;
    private GameObject leftController;
    private GameObject rightController;
    private InteractionsManager interactionsManager;
    private CloseInteractionsManager closeInteractions;
    private RangedInteractions rangedInteractionsManager;
    private GameObject screen;

	void Start () {
        if (isLogging)
        {
            string fileName = DateTime.Now.ToLongTimeString();
            fileName = participantName + "_" + fileName;
            fileName = fileName.Replace(':', ';');

            writer = File.CreateText(path + "/" + fileName + ".csv");

            string[] headers = new string[]
            {
                "Time since start",
                "Task",
                "Head position",
                "Head rotation",
                "Left controller position",
                "Left controller rotation",
                "Right controller position",
                "Right controller rotation",
                "Current head zone",
                "Current controller zone",
                "Interaction state",
                "Close interaction state",
                "Ranged interactions state",
                "Screen position",
                "Screen size",
                "Screen rotation",
                "Head distance from screen",
                "Left controller distance from screen",
                "Right controller distance from screen"
            };

            writer.WriteLine(string.Join("\t", headers));

            camera = Camera.main;
            leftController = VRTK_DeviceFinder.GetControllerLeftHand();
            rightController = VRTK_DeviceFinder.GetControllerRightHand();
            interactionsManager = InteractionsManager.Instance;
            closeInteractions = CloseInteractionsManager.Instance;
            rangedInteractionsManager = rightController.GetComponent<RangedInteractions>();
            screen = ScreenManager.Instance.Screen;

            InvokeRepeating("LogData", logInterval, logInterval);
        }
	}
	
	private void LogData()
    {
        string[] values = new string[]
        {
            Time.time.ToString(),
            currentTask,
            camera.transform.position.ToString("F4"),
            camera.transform.rotation.ToString("F4"),
            leftController.transform.position.ToString("F4"),
            leftController.transform.rotation.ToString("F4"),
            rightController.transform.position.ToString("F4"),
            rightController.transform.rotation.ToString("F4"),
            interactionsManager.HeadZone,
            interactionsManager.ControllerZone,
            interactionsManager.ActiveState,
            closeInteractions.ActiveState,
            rangedInteractionsManager.ActiveState,
            screen.transform.position.ToString("F4"),
            screen.transform.localScale.ToString("F4"),
            screen.transform.rotation.ToString("F4"),
            Vector3.Distance(screen.GetComponent<Collider>().ClosestPointOnBounds(camera.transform.position), camera.transform.position).ToString("F2"),
            Vector3.Distance(screen.GetComponent<Collider>().ClosestPointOnBounds(leftController.transform.position), leftController.transform.position).ToString("F2"),
            Vector3.Distance(screen.GetComponent<Collider>().ClosestPointOnBounds(rightController.transform.position), rightController.transform.position).ToString("F2"),
    };

        writer.WriteLine(string.Join("\t", values));
    }

    private void OnApplicationQuit()
    {
        if (isLogging)
        {
            writer.Close();
        }
    }
}
