﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VRTK;

public class InteractableChart : VRTK_InteractableObject
{
    [SerializeField] [Tooltip("The distance the controller needs to be pulled until a visualisation is created.")]
    private float distanceThreshold = 0.15f;
    [SerializeField] [Tooltip("The duration in seconds that the fade animation takes.")]
    private float animationDuration = 0.125f;

    private Vector3 startPos;
    private GameObject interactingController;
    private bool isBeingGrabbed = false;

    private Renderer meshRenderer;
    private Coroutine fadeAnimation;
    private float alpha;

    // Use this for initialization
    void Start()
    {
        InteractableObjectGrabbed += OnChartGrabbed;
        InteractableObjectUngrabbed += OnChartUngrabbed;

        meshRenderer = GetComponent<Renderer>();
        Color c = meshRenderer.material.color;
        alpha = c.a;
        c.a = 0;
        meshRenderer.material.color = c;

        Vector3 scale = transform.localScale;
        scale.z = InteractionsManager.Instance.ImmediateZoneThreshold * 2;
        transform.localScale = scale;
    }

    private void OnChartGrabbed(object sender, InteractableObjectEventArgs e)
    {
        InteractionsManager.Instance.GrabbingStarted();

        isBeingGrabbed = true;
        interactingController = e.interactingObject;
        startPos = interactingController.transform.position;
    }

    protected override void Update()
    {
        if (isBeingGrabbed)
        {
            float distance = Vector3.Distance(startPos, interactingController.transform.position);

            float vibrateAmount = 0.75f * (distance / distanceThreshold);
            VRTK_ControllerHaptics.TriggerHapticPulse(VRTK_ControllerReference.GetControllerReference(interactingController), vibrateAmount);
            if (distance > distanceThreshold)
            {
                isBeingGrabbed = false;

                ForceStopInteracting();

                SceneManager.Instance.CreateVisualisationAndAnimateToHand(interactingController, startPos, 0.2f);
                InteractionsManager.Instance.GrabbingFinished();
            }
        }
    }

    private void OnChartUngrabbed(object sender, InteractableObjectEventArgs e)
    {
        InteractionsManager.Instance.GrabbingFinished();

        isBeingGrabbed = false;
    }

    public void Enable()
    {
        fadeAnimation = StartCoroutine(Fade(alpha));
        isGrabbable = true;
    }

    public void Disable()
    {
        fadeAnimation = StartCoroutine(Fade(0));
        isGrabbable = false;
    }

    private IEnumerator Fade(float newAlpha)
    {
        float elapsedTime = 0;
        float oldAlpha = meshRenderer.material.color.a;
        while (elapsedTime < animationDuration)
        {
            elapsedTime += Time.deltaTime;

            Color col = meshRenderer.material.color;
            col.a = Mathf.Lerp(oldAlpha, newAlpha, elapsedTime / animationDuration);
            meshRenderer.material.color = col;
            yield return null;
        }

        Color c = meshRenderer.material.color;
        c.a = newAlpha;
        meshRenderer.material.color = c;
    }
}
