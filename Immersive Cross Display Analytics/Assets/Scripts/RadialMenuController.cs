﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VRTK;

public class RadialMenuController : VRTK_RadialMenuController
{

    bool touchpadClicked = false;
    
    protected override void DoTouchpadClicked(object sender, ControllerInteractionEventArgs e)
    {
        TouchAngleDeflection givenTouchAngleDeflection = CalculateAngle(e);
        if (givenTouchAngleDeflection.deflection <= menu.deadZone)
        {
            DoShowMenu(givenTouchAngleDeflection);
        }

        DoClickButton();
        menu.HoverButton(CalculateAngle(e));
        touchpadClicked = true;
    }

    protected override void DoTouchpadUnclicked(object sender, ControllerInteractionEventArgs e)
    {
        DoUnClickButton();
        DoHideMenu(false);
        touchpadClicked = false;
    }

    protected override void DoTouchpadTouched(object sender, ControllerInteractionEventArgs e)
    {
        touchpadTouched = true;
        //DoShowMenu(CalculateAngle(e));
    }

    protected override void DoTouchpadUntouched(object sender, ControllerInteractionEventArgs e)
    {
        touchpadTouched = false;
        //DoHideMenu(false);
    }

    protected override void DoChangeAngle(TouchAngleDeflection givenTouchAngleDeflection, object sender = null)
    {
        if (touchpadClicked)
        {
            currentTad = givenTouchAngleDeflection;

            menu.HoverButton(currentTad);
        }
    }
}
