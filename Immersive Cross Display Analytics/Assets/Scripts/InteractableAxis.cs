﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VRTK;

public class InteractableAxis : VRTK_InteractableObject {

    Axis AxisScript;
    InteractableHtmlAxisElement touchingHtmlAxisElementScript;

    void Start()
    {
        AxisScript = GetComponent<Axis>();
        InteractableObjectUngrabbed += OnAxisUngrabbed;
    }

    /// <summary>
    /// Override the method for when the user grabs this object so that the interactable script does not actually grab it, instead
    /// relying on the ImAxes WandController. This is to allow any Ungrabbed methods to function as per normal.
    /// </summary>
    /// <param name="currentGrabbingObject">The interacting object that is currently grabbing this Interactable Object.</param>
    public override void Grabbed(VRTK_InteractGrab currentGrabbingObject = null)
    {
        GameObject currentGrabbingGameObject = (currentGrabbingObject != null ? currentGrabbingObject.gameObject : null);
        ToggleEnableState(true);
        OnInteractableObjectGrabbed(SetInteractableObjectEvent(currentGrabbingGameObject));
    }

    private void OnAxisUngrabbed(object sender, InteractableObjectEventArgs e)
    {
        if (touchingHtmlAxisElementScript != null)
        {
            string name = AxisScript.axisName;
            Dimension dimension = touchingHtmlAxisElementScript.AxisDimension;
            WebSocketManager.Instance.SetDimension(name, dimension);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "HtmlAxisElement")
            touchingHtmlAxisElementScript = other.gameObject.GetComponent<InteractableHtmlAxisElement>();
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "HtmlAxisElement")
            touchingHtmlAxisElementScript = null;
    }
}
