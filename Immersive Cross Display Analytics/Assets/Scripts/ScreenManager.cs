﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;
using VRTK;

public enum SandDanceChartType
{
    Bar,
    Column,
    Custom,
    Density,
    Grid,
    Lime,
    Links,
    Poisson,
    Radial,
    Random,
    Scatter,
    Scatter3D,
    Spiral,
    Stacks,
    Violin,
    Xband,
    Yband
}

public class ScreenManager : MonoBehaviour {

    public static ScreenManager Instance { get; private set; }

    public List<GameObject> Shapes { get; private set; }
    public List<int> SelectedIndices { get; private set; }
    public string ChartX { get; private set; }
    public string ChartY { get; private set; }
    public string ChartZ { get; private set; }
    public Color ChartColor { get; private set; }
    public string ChartColorBy { get; private set; }
    public Color[] ChartPalette { get; private set; }
    public SandDanceChartType ChartType { get; private set; }

    private int[] selectedIndicesFromSandDance;

    [Header("Child References")]
    [Tooltip("The game object which represents the screen.")]
    public GameObject Screen;
    [Tooltip("The game object which represents the chart.")]
    public GameObject Chart;
    [Tooltip("The game object which contains the collider which is used to grab the chart.")]
    public GameObject ChartCollider;
    [Tooltip("The game object which contains all elements/shapes in the chart.")]
    public GameObject Elements;
    [Tooltip("The game object which renders the convex mesh used when making selections.")]
    public GameObject ConvexMesh;

    private bool isChangingShapes = false;
    private bool isChangingElements = false;
    private bool isSendingSelection = false;
    private bool isChangingSelection = false;
    private ShapeDetails[] shapesDetails;
    private ShapeRanges shapesRanges;

    [Header("Size Parameters")]
    [SerializeField] [Tooltip("The depth of the shapes/units on the screen.")]
    private float shapeDepth = 0.00075f;
    [SerializeField] [Tooltip("The depth of the HTML buttons on the screen.")]
    private float htmlElementDepth = 0.001f;
    [SerializeField] [Tooltip("The amount to scale up the HTML axis buttons up by to make them easier to target.")]
    private float axisElementScaleMultiplier = 1.5f;
    [SerializeField] [Tooltip("The amount to multiply every shape and HTML element on the screen by as the user approaches close enough to it.")]
    private float screenExtendMultiplier = 25f;

    [Header("HTML Elements")]
    [SerializeField] [Tooltip("The IDs of any HTML elements to be requested from SandDance and represented on screen as game objects.")]
    private string[] htmlElementIdsToShow;
    [SerializeField] [Tooltip("The classes of any HTML elements to be requested from SandDance and represented on screen as game objects.")]
    private string[] htmlElementClassesToShow;
    [SerializeField] [Tooltip("The titles of any HTML elements that are to be set as 'restricted', that is they are shown in a different colour as a warning not to use them.")]
    private string[] htmlElementIdsToRestrict;
    [SerializeField] [Tooltip("The IDs of any HTML elements that are to be set as 'restricted', that is they are shown in a different colour as a warning not to use them.")]
    private string[] htmlElementTitlesToRestrict;
    [SerializeField] [Tooltip("The colour to set restricted HTML elements to.")]
    private Color htmlElementRestrictedColor = new Color(200, 0, 0, 50);
    [SerializeField] [Tooltip("The prefab instantiated for each shape.")]
    private GameObject shapePrefab;
    [SerializeField] [Tooltip("The prefab instantiated for each HTML element but not axis elements.")]
    private GameObject htmlElementPrefab;
    [SerializeField] [Tooltip("The prefab instantiated for each HTML axis element.")]
    private GameObject htmlAxisElementPrefab;
    [SerializeField] [Tooltip("The prefab instantiated for each panel which contains the shaders to hide fragments behind it.")]
    private GameObject htmlPanelPrefab;

    private Dictionary<string, GameObject> htmlElementsDict;
    private HtmlElement[] htmlElements;

    private float screenHeight;
    private float screenWidth;
    private HtmlElement windowElement;
    private HtmlElement chartElement;
    
    Coroutine shapesDepthCoroutine;
    Coroutine htmlElementsDepthCoroutine;

    [SerializeField] [Tooltip("The material for the chart")]
    private Material shapesMaterial;
    private MeshFilter shapesFilter;
    private MeshRenderer shapesRenderer;
    private Mesh shapesMesh;

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
    }

    // Use this for initialization
    private void Start()
    {
        WebSocketManager.Instance.GetChartDetailsCompleted += ChartDetailsReceived;
        WebSocketManager.Instance.GetShapesDetailsCompleted += ShapesDetailsReceived;
        WebSocketManager.Instance.GetHtmlElementsCompleted += HtmlElementsReceived;
        WebSocketManager.Instance.GetSelectedIndicesCompleted += SelectedIndicesReceived;

        screenWidth = Screen.transform.localScale.x;
        screenHeight = Screen.transform.localScale.y;

        Shapes = new List<GameObject>();
        SelectedIndices = new List<int>();
        htmlElementsDict = new Dictionary<string, GameObject>();
        
        screenWidth = Screen.transform.localScale.x;
        screenHeight = Screen.transform.localScale.y;

        shapesMesh = new Mesh();
        shapesFilter = Chart.AddComponent<MeshFilter>();
        shapesFilter.mesh = shapesMesh;
        shapesRenderer = Chart.AddComponent<MeshRenderer>();
        shapesRenderer.material = shapesMaterial;
    }
    
    private void ChartDetailsReceived(object sender, ChartDetailsEventArgs e)
    {
        ChartX = e.X;
        ChartY = e.Y;
        ChartZ = e.Z;
        ChartColor = e.Color ;
        ChartColorBy = e.ColorBy;
        ChartPalette = e.ColorPalette;
        ChartType = (SandDanceChartType)e.ChartType;
    }

    private void ShapesDetailsReceived(object sender, ShapesDetailsEventArgs e)
    {
        if (shapesDetails == null)
        {
            shapesDetails = e.Details;
            shapesRanges = e.Ranges;
            isChangingShapes = true;
        }
        // If the positions of all of the shapes don't change, then don't reposition them
        else
        {
            float[] xArr1 = shapesDetails.Select(x => x.x).ToArray();
            float[] xArr2 = e.Details.Select(x => x.x).ToArray();
            float[] yArr1 = shapesDetails.Select(x => x.y).ToArray();
            float[] yArr2 = e.Details.Select(x => x.y).ToArray();

            if (!(xArr1.SequenceEqual(xArr2) && yArr1.SequenceEqual(yArr2)))
            {
                shapesDetails = e.Details;
                shapesRanges = e.Ranges;
                isChangingShapes = true;
            }
        }
    }

    private void HtmlElementsReceived(object sender, ElementPositionsEventArgs e)
    {
        windowElement = e.Window;
        chartElement = e.Chart;
        htmlElements = e.Positions;

        isChangingElements = true;
    }

    private void SelectedIndicesReceived(object sender, SelectedIndicesEventArgs e)
    {
        selectedIndicesFromSandDance = e.SelectedIndices;

        isChangingSelection = true;
    }

    private void Update()
    {
        if (isChangingElements)
        {
            PositionHtmlElements();
            isChangingElements = false;
        }
        if (isChangingShapes)
        {
            PositionShapes();
            isChangingShapes = false;
        }
        if (isChangingSelection)
        {
            ResetShapeSelections();
            ShapesSelected(selectedIndicesFromSandDance);
            isChangingSelection = false;
        }
    }

    private IEnumerator AnimateDepthChange(GameObject obj, float depth, float time)
    {
        float elapsedTime = 0;
        float startDepth = obj.transform.localScale.z;
        while (elapsedTime < time)
        {
            float newDepth = Mathf.Lerp(startDepth, depth, elapsedTime / time);
            obj.transform.localScale = new Vector3(obj.transform.localScale.x, obj.transform.localScale.y, newDepth);
            elapsedTime += Time.deltaTime;
            yield return null;
        }
        obj.transform.localScale = new Vector3(obj.transform.localScale.x, obj.transform.localScale.y, depth);
    }

    public void ExtendElements()
    {
        if (shapesDepthCoroutine != null || htmlElementsDepthCoroutine != null)
        {
            //StopCoroutine(shapesDepthCoroutine);
            StopCoroutine(htmlElementsDepthCoroutine);
        }

        //shapesDepthCoroutine = StartCoroutine(AnimateDepthChange(Chart, screenExtendMultiplier, 0.25f));
        htmlElementsDepthCoroutine = StartCoroutine(AnimateDepthChange(Elements, screenExtendMultiplier, 0.25f));
    }

    public void RetractElements()
    {
        if (shapesDepthCoroutine != null || htmlElementsDepthCoroutine != null)
        {
            //StopCoroutine(shapesDepthCoroutine);
            StopCoroutine(htmlElementsDepthCoroutine);
        }

        //shapesDepthCoroutine = StartCoroutine(AnimateDepthChange(Chart, 1, 0.25f));
        htmlElementsDepthCoroutine =  StartCoroutine(AnimateDepthChange(Elements, 1, 0.25f));
    }

    private void PositionShapes()
    {
        // Instantiate shapes
        if (Shapes.Count == 0)
        {
            for (int i = 0; i < shapesDetails.Length; i++)
            {
                GameObject shape = Instantiate(shapePrefab);
                shape.transform.SetParent(Chart.transform);
                shape.GetComponent<InteractableShape>().SetIndex(i);
                Shapes.Add(shape);
            }
        }
        // Position, scale and rotate the shapes
        for (int i = 0; i < Shapes.Count; i++)
        {
            GameObject shape = Shapes[i];

            // Max range can be off if right hand side not filled, take absolute of left hand side
            float normalisedX = Normalise(-0.5f, 0.5f, shapesRanges.xMin, shapesRanges.xMax, shapesDetails[i].x);
            float normalisedY = Normalise(-0.5f, 0.5f, shapesRanges.yMin, shapesRanges.yMax, shapesDetails[i].y);
            shape.transform.localPosition = new Vector3(normalisedX, normalisedY, -shapeDepth / 2);
            
            float normalisedWidth = Normalise(0, 1, 0, shapesRanges.xMax * 2, shapesDetails[i].width);
            float normalisedHeight = Normalise(0, 1, 0, shapesRanges.yMax * 2, shapesDetails[i].height);
            shape.transform.localScale = new Vector3(normalisedWidth, normalisedHeight, shapeDepth);

            shape.transform.rotation = Chart.transform.rotation;

            // If the shape is to be positioned outside of the screen, make it invisible
            if (0.5f < normalisedX || normalisedX < -0.5f || 0.5f < normalisedY || normalisedY < -0.5f)
            {
                shape.transform.localScale = Vector3.zero;
            }
        }

        UpdateShapesMesh();
    }
    
    private void UpdateShapesMesh()
    {
        shapesMesh.Clear();

        Vector3[] vertices = new Vector3[Shapes.Count * 8];
        int[] triangles = new int[Shapes.Count * 36];

        for (int i = 0; i < Shapes.Count; i++)
        {
            GameObject shape = Shapes[i];

            float posX = shape.transform.localPosition.x;
            float posY = shape.transform.localPosition.y;
            float sizeX = shape.transform.localScale.x / 2;
            float sizeY = shape.transform.localScale.y / 2;
            float sizeZ = shape.transform.localScale.z;

            int verticesOffset = i * 8;
            vertices[verticesOffset + 0] = new Vector3(posX - sizeX, posY - sizeY, sizeZ);
            vertices[verticesOffset + 1] = new Vector3(posX + sizeX, posY - sizeY, sizeZ);
            vertices[verticesOffset + 2] = new Vector3(posX + sizeX, posY + sizeY, sizeZ);
            vertices[verticesOffset + 3] = new Vector3(posX - sizeX, posY + sizeY, sizeZ);
            vertices[verticesOffset + 4] = new Vector3(posX - sizeX, posY + sizeY, -sizeZ);
            vertices[verticesOffset + 5] = new Vector3(posX + sizeX, posY + sizeY, -sizeZ);
            vertices[verticesOffset + 6] = new Vector3(posX + sizeX, posY - sizeY, -sizeZ);
            vertices[verticesOffset + 7] = new Vector3(posX - sizeX, posY - sizeY, -sizeZ);

            int trianglesOffset = i * 36;
            // Front face
            triangles[trianglesOffset + 0] = verticesOffset + 0;
            triangles[trianglesOffset + 1] = verticesOffset + 2;
            triangles[trianglesOffset + 2] = verticesOffset + 1;
            triangles[trianglesOffset + 3] = verticesOffset + 0;
            triangles[trianglesOffset + 4] = verticesOffset + 3;
            triangles[trianglesOffset + 5] = verticesOffset + 2;
            // Top face
            triangles[trianglesOffset + 6] = verticesOffset + 2;
            triangles[trianglesOffset + 7] = verticesOffset + 3;
            triangles[trianglesOffset + 8] = verticesOffset + 4;
            triangles[trianglesOffset + 9] = verticesOffset + 2;
            triangles[trianglesOffset + 10] = verticesOffset + 4;
            triangles[trianglesOffset + 11] = verticesOffset + 5;
            // Left face
            triangles[trianglesOffset + 12] = verticesOffset + 0;
            triangles[trianglesOffset + 13] = verticesOffset + 7;
            triangles[trianglesOffset + 14] = verticesOffset + 4;
            triangles[trianglesOffset + 15] = verticesOffset + 0;
            triangles[trianglesOffset + 16] = verticesOffset + 4;
            triangles[trianglesOffset + 17] = verticesOffset + 3;
            // Right face
            triangles[trianglesOffset + 18] = verticesOffset + 1;
            triangles[trianglesOffset + 19] = verticesOffset + 2;
            triangles[trianglesOffset + 20] = verticesOffset + 5;
            triangles[trianglesOffset + 21] = verticesOffset + 1;
            triangles[trianglesOffset + 22] = verticesOffset + 5;
            triangles[trianglesOffset + 23] = verticesOffset + 6;
            // Bottom face
            triangles[trianglesOffset + 24] = verticesOffset + 0;
            triangles[trianglesOffset + 25] = verticesOffset + 6;
            triangles[trianglesOffset + 26] = verticesOffset + 7;
            triangles[trianglesOffset + 27] = verticesOffset + 0;
            triangles[trianglesOffset + 28] = verticesOffset + 1;
            triangles[trianglesOffset + 29] = verticesOffset + 6;
            // Back face
            triangles[trianglesOffset + 30] = verticesOffset + 5;
            triangles[trianglesOffset + 31] = verticesOffset + 4;
            triangles[trianglesOffset + 32] = verticesOffset + 7;
            triangles[trianglesOffset + 33] = verticesOffset + 5;
            triangles[trianglesOffset + 34] = verticesOffset + 7;
            triangles[trianglesOffset + 35] = verticesOffset + 6;
        }

        shapesMesh.vertices = vertices;
        shapesMesh.triangles = triangles;
        shapesMesh.normals = new Vector3[Shapes.Count * 8];
    }

    private void SetShapeColour(int index, bool selected)
    {
        Vector3[] normals = shapesMesh.normals;
        
        for (int i = 0; i < 8; i++)
        {
            normals[index * 8 + i].x = selected ? 1 : 0;
        }

        shapesMesh.normals = normals;
    }

    private void SetShapeColour(int[] indices, bool selected)
    {
        Vector3[] normals = shapesMesh.normals;

        foreach (int index in indices)
        {
            for (int i = 0; i < 8; i++)
            {
                normals[index * 8 + i].x = selected ? 1 : 0;
            }
        }

        shapesMesh.normals = normals;
    }

    private void ResetShapeColours()
    {
        shapesMesh.normals = new Vector3[Shapes.Count * 8];
    }

    private void PositionHtmlElements()
    {        
        // Position the chart within the screen
        float[] chartCoords = CalculatePosition(chartElement);

        Chart.transform.SetParent(Screen.transform);
        Chart.transform.localPosition = new Vector3(chartCoords[0], chartCoords[1], -0.001f);

        // Calculate the actual size of the chart area
        float[] chartSize = CalculateSize(chartElement);
        Chart.transform.localScale = new Vector3(chartSize[0], chartSize[1], Chart.transform.localScale.z);

        Elements.transform.SetParent(Screen.transform);
        Elements.transform.localPosition = new Vector3(0, 0, -0.0075f);
        Elements.transform.localScale = new Vector3(1, 1, Elements.transform.localScale.z);

        // Set the interactable chart to the same dimensions
        ChartCollider.transform.localPosition = new Vector3(chartCoords[0], chartCoords[1], -0.001f);
        ChartCollider.transform.localScale = new Vector3(chartSize[0], chartSize[1], ChartCollider.transform.localScale.z);

        List<string> updatedElements = new List<string>();

        foreach (HtmlElement el in htmlElements)
        {
            // Skip if this element is not to be shown
            if (!htmlElementIdsToShow.Contains(el.id) && !htmlElementClassesToShow.Contains(el.className))
            {
                continue;
            }

            string key = GenerateHtmlElementKeyId(el);
            updatedElements.Add(key);

            // Instantiate new objects if the element is not already stored
            if (!htmlElementsDict.Keys.Contains(key))
            {
                GameObject element;
                InteractableHtmlElement elementScript;

                // If the HTML element is an axis button, set its appropriate dimension
                if (el.id == "xButton" || el.id == "yButton" || el.id == "zButton")
                {
                    element = Instantiate(htmlAxisElementPrefab);
                    InteractableHtmlAxisElement axisScript = element.GetComponent<InteractableHtmlAxisElement>();

                    if (el.id == "xButton")
                    {
                        axisScript.AxisName = ChartX;
                        axisScript.AxisDimension = Dimension.X;
                    }
                    else if (el.id == "yButton")
                    {
                        axisScript.AxisName = ChartY;
                        axisScript.AxisDimension = Dimension.Y;
                    }
                    else if (el.id == "zButton")
                    {
                        axisScript.AxisName = ChartZ;
                        axisScript.AxisDimension = Dimension.Z;
                    }
                    elementScript = axisScript;
                }
                // If the HTML element is a panel
                else if (el.className == "appBody flexRows panel dockedPanel")
                {
                    element = Instantiate(htmlPanelPrefab);
                    elementScript = element.GetComponent<InteractableHtmlElement>();

                }
                else
                {
                    element = Instantiate(htmlElementPrefab);
                    elementScript = element.GetComponent<InteractableHtmlElement>();
                }

                elementScript.SetHtmlElementValues(el);
                htmlElementsDict.Add(key, element);
                element.transform.SetParent(Elements.transform);

                float[] elementCoords = CalculatePosition(el);
                element.transform.localPosition = new Vector3(elementCoords[0], elementCoords[1], -htmlElementDepth / 2.5f);

                element.transform.rotation = Screen.transform.rotation;

                float[] elementSize = CalculateSize(el);
                element.transform.localScale = new Vector3(elementSize[0], elementSize[1], htmlElementDepth);

                // If the element is an axis, scale it up so it is easier to target
                if (el.id == "xButton" || el.id == "yButton" || el.id == "zButton")
                {
                    element.transform.localScale = element.transform.localScale * axisElementScaleMultiplier;
                }
                else if (el.className == "appBody flexRows panel dockedPanel")
                {
                    // Set it to the same position as all of the shapes
                    element.transform.SetParent(Chart.transform);
                    Vector3 position = element.transform.localPosition;
                    position.z = 0;
                    element.transform.localPosition = position;

                    // Unparent if the HTML element is a panel so that it does not retract/extend
                    element.transform.parent = null;

                    // Make sure that it's not thick
                    Vector3 scale = element.transform.localScale;
                    scale.z = htmlElementDepth;
                    element.transform.localScale = scale;
                }

                // Gameobjects are unresponsive with VRTK unless we move them a tiny bit
                //element.transform.localScale = element.transform.localScale + Vector3.forward * 0.000001f;

                // If the element is a restricted element, change its colour
                if (htmlElementIdsToRestrict.Contains(el.id) || htmlElementTitlesToRestrict.Contains(el.title))
                {
                    element.GetComponent<Renderer>().material.color = htmlElementRestrictedColor;

                    // Disable its associated highlighter
                    element.GetComponent<VRTK_InteractObjectHighlighter>().enabled = false;
                }
            }
        }

        string[] idsToRemove = htmlElementsDict.Keys.Except(updatedElements).ToArray();

        foreach (string id in idsToRemove)
        {
            Destroy(htmlElementsDict[id]);
            htmlElementsDict.Remove(id);
        }
    }

    /// <summary>
    /// Generates a (mostly) unique id that can be used to identify a specific HTML element based upon its properties
    /// </summary>
    /// <param name="pos"></param>
    /// <returns></returns>
    private string GenerateHtmlElementKeyId(HtmlElement pos)
    {
        return pos.id + pos.className + pos.top + pos.left + pos.width + pos.height;
    }

    private void LateUpdate()
    {
        if (isSendingSelection)
        {
            WebSocketManager.Instance.SetSelectedIndices(SelectedIndices.ToArray());

            isSendingSelection = false;
        }
    }

    private float[] CalculatePosition(HtmlElement pos)
    {
        float xCenterPixels = pos.left + pos.width / 2;
        float yCenterPixels = pos.top + pos.height / 2;

        float xCenterMeters = Normalise(-0.5f, 0.5f, 0, windowElement.width, xCenterPixels);
        float yCenterMeters = Normalise(-0.5f, 0.5f, 0, windowElement.height, yCenterPixels);

        // Origin point in HTML is top left, with elements going downwards increasing in the y-axis
        // Origin point in Unity is in the middle, with elements going downwards decreasing in the y-axis
        // Therefore we negate the y position to resolve this
        return new float[] { xCenterMeters, -yCenterMeters };
    }

    private float[] CalculateSize(HtmlElement pos, bool isWorldSpace=false)
    {
        float widthMeters, heightMeters;
        if (isWorldSpace)
        {
            widthMeters = Normalise(0, screenWidth, 0, windowElement.width, pos.width);
            heightMeters = Normalise(0, screenHeight, 0, windowElement.height, pos.height);
        }
        else
        {
            widthMeters = Normalise(0, 1, 0, windowElement.width, pos.width);
            heightMeters = Normalise(0, 1, 0, windowElement.height, pos.height);
        }

        return new float[] { widthMeters, heightMeters };
    }


    private float Normalise(float minRange, float maxRange, float min, float max, float value)
    {
        return (maxRange - minRange) * (value - min) / (max - min) + minRange;
    }
    
    public bool HasInitialised()
    {
        return (htmlElementsDict.Values.Count > 0);
    }


    public void ShapeSelected(int index)
    {
        if (!SelectedIndices.Contains(index))
        {
            SelectedIndices.Add(index);

            isSendingSelection = true;

            SetShapeColour(index, true);
        }
    }

    public void ShapesSelected(int[] indices)
    {
        foreach (int index in indices)
        {
            if (!SelectedIndices.Contains(index))
            {
                SelectedIndices.Add(index);
                Shapes[index].GetComponent<InteractableShape>().SetShapeSelected();
            }
        }

        isSendingSelection = true;

        SetShapeColour(indices, true);
    }

    public void ShapeDeselected(int index)
    {
        if (SelectedIndices.Contains(index))
        {
            SelectedIndices.Remove(index);

            isSendingSelection = true;

            SetShapeColour(index, false);
        }
    }

    public void ShapesDeselected(int[] indices)
    {
        foreach (int index in indices)
        {
            if (SelectedIndices.Contains(index))
            {
                SelectedIndices.Remove(index);
                Shapes[index].GetComponent<InteractableShape>().SetShapeDeselected();
            }
        }

        isSendingSelection = true;

        SetShapeColour(indices, false);
    }

    public void ResetShapeSelections()
    {
        foreach (GameObject shape in Shapes)
        {
            shape.GetComponent<InteractableShape>().SetShapeDeselected();
        }

        isSendingSelection = true;
        ResetShapeColours();

        SelectedIndices.Clear();
    }

    public void ClosePopupWindows()
    {
        WebSocketManager.Instance.CloseHtmlPopupMenus();
    }
}
