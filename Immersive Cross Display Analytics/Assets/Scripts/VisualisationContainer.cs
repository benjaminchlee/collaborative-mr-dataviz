﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class VisualisationContainer : MonoBehaviour, IAnimatable
{
    public GameObject GetVisualisationObject()
    {
        // Get the child axes of the empty gameobject
        List<GameObject> pulledAxes = new List<GameObject>();
        foreach (Transform t in transform)
        {
            pulledAxes.Add(t.gameObject);
        }

        // Find the visualisation gameobject for the axes depending on the number of axes in it
        int axesCount = transform.childCount;
        GameObject[] visualisations = new GameObject[0];

        if (axesCount == 1)
        {
            visualisations = GameObject.FindGameObjectsWithTag("Visualisation");
        }
        else if (axesCount == 2)
        {
            visualisations = GameObject.FindGameObjectsWithTag("Scatterplot2D");
        }
        else if (axesCount == 3)
        {
            visualisations = GameObject.FindGameObjectsWithTag("Scatterplot3D");
        }

        foreach (GameObject vis in visualisations)
        {
            List<Axis> axes = vis.GetComponent<Visualization>().axes;
            GameObject[] visAxes = axes.Select(x => x.gameObject).ToArray();

            // If the current visualisation is the correct one, remove the parent object and grab the visualisation
            if (pulledAxes.Intersect(visAxes).Count() == pulledAxes.Count)
            {
                return vis;
            }
        }

        return null;
    }

    public void AnimateTowardsObject(Vector3 startPos, GameObject targetObj, float time)
    {
        StartCoroutine(AnimateToObject(startPos, targetObj, time));
    }

    IEnumerator AnimateToObject(Vector3 startPos, GameObject targetObj, float time)
    {
        transform.position = startPos;
        float elapsedTime = 0;
        Rigidbody rb = gameObject.GetComponent<Rigidbody>();
        while (elapsedTime < time)
        {
            rb.MovePosition(Vector3.Lerp(transform.position, targetObj.transform.position, elapsedTime / time));
            elapsedTime += Time.deltaTime;
            yield return new WaitForFixedUpdate();
        }
    }

    public void AnimateTowardsControllerAndGrab(Vector3 startPos, GameObject controller, float time)
    {
        StartCoroutine(AnimateToControllerAndGrab(startPos, controller, time));
    }

    IEnumerator AnimateToControllerAndGrab(Vector3 startPos, GameObject controller, float time)
    {
        yield return AnimateToObject(startPos, controller, time);
        GameObject vis = GetVisualisationObject();
        transform.DetachChildren();
        Destroy(gameObject);
        controller.transform.GetComponentInParent<WandController>().PropergateOnGrab(vis);
        yield return null;
    }

    public void AnimateTowardsPointAndDestroy(Vector3 targetPos, float time)
    {
        StartCoroutine(AnimateToPointAndDestroy(targetPos, time));
    }

    IEnumerator AnimateToPointAndDestroy(Vector3 targetPos, float time)
    {
        float elapsedTime = 0;
        Rigidbody rb = gameObject.GetComponent<Rigidbody>();
        while (elapsedTime < time)
        {
            rb.MovePosition(Vector3.Lerp(transform.position, targetPos, elapsedTime / time));
            elapsedTime += Time.deltaTime;
            yield return new WaitForFixedUpdate();
        }
        GameObject vis = GetVisualisationObject();
        transform.DetachChildren();
        Destroy(gameObject);
        SceneManager.Instance.RemoveVisualisation(vis.GetComponent<Visualization>());
    }
}
