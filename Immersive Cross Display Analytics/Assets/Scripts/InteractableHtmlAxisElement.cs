﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VRTK;

public class InteractableHtmlAxisElement : InteractableHtmlElement {

    public string AxisName { get; set; }
    public Dimension AxisDimension { get; set; }

    [SerializeField] [Tooltip("The distance the controller needs to be pulled until a visualisation is created.")]
    private float distanceThreshold = 0.15f;

    private bool isBeingGrabbed;
    private bool isAxisCreated;
    private GameObject interactingController;
    private Vector3 startPos;

    override protected void OnElementGrabbed(object sender, InteractableObjectEventArgs e)
    {
        base.OnElementGrabbed(sender, e);

        if (AxisName != null)
        {
            isBeingGrabbed = true;
            interactingController = e.interactingObject;
            startPos = interactingController.transform.position;
        }
    }
    
    override protected void Update () {
        base.Update();

		if (isBeingGrabbed)
        {
            //float distance = Mathf.Max(interactingController.transform.InverseTransformPoint(startPos).z, 0);
            float distance = Vector3.Distance(startPos, interactingController.transform.position);

            float vibrateAmount = 0.75f * (distance / distanceThreshold);
            VRTK_ControllerHaptics.TriggerHapticPulse(VRTK_ControllerReference.GetControllerReference(interactingController), vibrateAmount);

            if (distance > distanceThreshold)
            {
                isAxisCreated = true;
                ForceStopInteracting();

                SceneManager.Instance.CreateAxisAndAnimateToHand(interactingController, AxisName, transform.position, 0.2f);

                isBeingGrabbed = false;
            }
        }
	}

    override protected void OnElementUngrabbed(object sender, InteractableObjectEventArgs e)
    {
        // Because the event is emitted when we force stop interacting, don't call this when that happens
        if (!isAxisCreated)
            base.OnElementUngrabbed(sender, e);

        isAxisCreated = false;
        isBeingGrabbed = false;
    }
}
