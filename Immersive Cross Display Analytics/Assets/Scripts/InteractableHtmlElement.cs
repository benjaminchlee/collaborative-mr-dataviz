﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VRTK;

public class InteractableHtmlElement : VRTK_InteractableObject {

    public HtmlElement Element { get; private set; }

    public string Id;// { get; private set; }
    public string TagName;// { get; private set; }
    public string ClassName;// { get; private set; }
    public string Title;// { get; private set; }
    public float Left;// { get; private set; }
    public float Top;// { get; private set; }
    public float Width;// { get; private set; }
    public float Height;// { get; private set; }

    bool isForceUngrabbed;

    virtual protected void Start()
    {
        InteractableObjectGrabbed += OnElementGrabbed;
        InteractableObjectUngrabbed += OnElementUngrabbed;
    }

    public void SetHtmlElementValues(HtmlElement el)
    {
        Element = el;

        Id = el.id;
        TagName = el.tagName;
        ClassName = el.className;
        Title = el.title;
        Left = el.left;
        Top = el.top;
        Width = el.width;
        Height = el.height;
    }

    virtual protected void OnElementGrabbed(object sender, InteractableObjectEventArgs e)
    {
        InteractionsManager.Instance.GrabbingStarted();
    }


    virtual protected void OnElementUngrabbed(object sender, InteractableObjectEventArgs e)
    {
        InteractionsManager.Instance.GrabbingFinished();

        Click();
    }

    public void Click()
    {
        WebSocketManager.Instance.HtmlElementClicked(Element);
    }
}
