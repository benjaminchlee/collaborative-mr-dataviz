﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VRTK;

public class Brush : MonoBehaviour {

    BrushMode Mode = BrushMode.Select;
    GameObject InteractingController;

    public enum BrushMode
    {
        Select,
        Deselect
    }

    public void SetBrushMode(BrushMode mode)
    {
        Mode = mode;
    }

    public void SetInteractingController(GameObject interactingController)
    {
        InteractingController = interactingController;
    }

    void OnTriggerEnter(Collider col)
    {
        if (col.gameObject.tag == "Shape")
        {
            InteractableShape shapeScript = col.gameObject.GetComponent<InteractableShape>();

            // Only select/unselect the shape if it is originally unselected/selected
            if (Mode == BrushMode.Select && !shapeScript.IsSelected)
                shapeScript.ShapeSelected();
            else if (Mode == BrushMode.Deselect && shapeScript.IsSelected)
                shapeScript.ShapeDeselected();
            else
                return;

            VRTK_ControllerHaptics.TriggerHapticPulse(VRTK_ControllerReference.GetControllerReference(InteractingController), 0.15f);
            
        }
    }
}
