﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Events;
using VRTK;


public interface IAnimatable
{
    void AnimateTowardsObject(Vector3 startPos, GameObject targetObj, float time);
    void AnimateTowardsControllerAndGrab(Vector3 startPos, GameObject controller, float time);
    void AnimateTowardsPointAndDestroy(Vector3 targetPos, float time);
}

public enum Dimension
{
    X,
    Y,
    Z
}

public class SceneManager : MonoBehaviour
{

    public List<Axis> sceneAxes { get; internal set; }

    public DataBinding.DataObject dataObject;

    public class OnAxisAddedEvent : UnityEvent<Axis> { }
    public OnAxisAddedEvent OnAxisAdded = new OnAxisAddedEvent();
    public class OnAxisRemovedEvent : UnityEvent<Axis> { }
    public OnAxisRemovedEvent OnAxisRemoved = new OnAxisRemovedEvent();

    [SerializeField]
    GameObject axisPrefab;

    [Header("Data Source")]

    [SerializeField]
    TextAsset sourceData;

    [SerializeField]
    DataObjectMetadata metadata;


    static SceneManager _instance;
    public static SceneManager Instance
    {
        get { return _instance ?? (_instance = FindObjectOfType<SceneManager>()); }
    }


    void Start()
    {
        sceneAxes = new List<Axis>();
        dataObject = new DataBinding.DataObject(sourceData.text, metadata);

        // setup default visual settings

        VisualisationAttributes.Instance.sizes = Enumerable.Range(0, SceneManager.Instance.dataObject.DataPoints).Select(_ => 1f).ToArray();

        List<float> categories = SceneManager.Instance.dataObject.getNumberOfCategories(VisualisationAttributes.Instance.ColoredAttribute);
        int nbCategories = categories.Count;
        Color[] palette = Colors.generateColorPalette(nbCategories);

        Dictionary<float, Color> indexCategoryToColor = new Dictionary<float, Color>();
        for (int i = 0; i < categories.Count; i++)
        {
            indexCategoryToColor.Add(categories[i], palette[i]);
        }

        VisualisationAttributes.Instance.colors = Colors.mapColorPalette(SceneManager.Instance.dataObject.getDimension(VisualisationAttributes.Instance.ColoredAttribute), indexCategoryToColor);

        // create the axis

        //for (int i = 0; i < dataObject.Identifiers.Length; ++i)
        //{
        //    Vector3 v = new Vector3(1.352134f - (i % 7) * 0.35f, 1.506231f - (i / 7) / 2f, 0f);// -0.4875801f);
        //    GameObject obj = (GameObject)Instantiate(axisPrefab);
        //    obj.transform.position = v;
        //    Axis axis = obj.GetComponent<Axis>();
        //    axis.Init(dataObject, i, true);
        //    axis.InitOrigin(v, obj.transform.rotation);
        //    axis.tag = "Axis";

        //    AddAxis(axis);
        //}

    }

    public GameObject CreateAxis(string dimension)
    {
        int index = dataObject.dimensionToIndex(dimension);
        return CreateAxis(index);
    }

    public GameObject CreateAxis(int index)
    {
        GameObject obj = Instantiate(axisPrefab);
        Axis axis = obj.GetComponent<Axis>();
        axis.Init(dataObject, index, false);
        axis.tag = "Axis";
        AddAxis(axis);

        return obj;
    }

    public void CreateAxisInHand(GameObject interactingController, string dimension)
    {
        int index = dataObject.dimensionToIndex(dimension);
        CreateAxisInHand(interactingController, index);
    }

    public void CreateAxisInHand(GameObject interactingController, int index)
    {
        GameObject obj = CreateAxis(index);

        SnapGameObjectToHand(obj, interactingController);
        obj.transform.LookAt(Camera.main.transform);
    }

    private void SnapGameObjectToHand(GameObject obj, GameObject interactingController)
    {
        obj.transform.position = interactingController.transform.position;

        // Delete all previous autograb scripts
        VRTK_ObjectAutoGrab[] autoGrabScripts = interactingController.GetComponents<VRTK_ObjectAutoGrab>();
        foreach (VRTK_ObjectAutoGrab script in autoGrabScripts)
        {
            Destroy(script);
        }

        // Snap to hand
        VRTK_ObjectAutoGrab autoGrab = interactingController.AddComponent<VRTK_ObjectAutoGrab>();
        autoGrab.objectToGrab = obj.GetComponent<VRTK_InteractableObject>();

        interactingController.GetComponent<VRTK_InteractGrab>().AttemptGrab();
    }


    public void CreateAxisAndAnimateToHand(GameObject interactingController, string dimension, Vector3 startPos, float time)
    {
        int index = dataObject.dimensionToIndex(dimension);
        CreateAxisAndAnimateToHand(interactingController, index, startPos, time);
    }

    public void CreateAxisAndAnimateToHand(GameObject interactingController, int index, Vector3 startPos, float time)
    {
        GameObject obj = CreateAxis(index);

        AnimateGameObjectToHand(obj, interactingController, startPos, time);
        obj.transform.LookAt(Camera.main.transform);
    }

    private void AnimateGameObjectToHand(GameObject obj, GameObject interactingController, Vector3 startPos, float time)
    {
        IAnimatable animateScript = obj.GetComponent<IAnimatable>();
        animateScript.AnimateTowardsControllerAndGrab(startPos, interactingController, time);
    }

    public void AddAxis(Axis axis)
    {
        sceneAxes.Add(axis);
        OnAxisAdded.Invoke(axis);
    }

    public void RemoveAxis(Axis axis)
    {
        OnAxisRemoved.Invoke(axis);
        sceneAxes.Remove(axis);
        Destroy(axis.gameObject);
        
        // Destroy the visualisation gameobjects tied to this axis
        foreach (Visualization vis in axis.correspondingVisualizations())
        {
            Destroy(vis.gameObject);

            // Destroy the SPLOM3D gameobjects tied to this visualisation
            foreach (SPLOM3D splom3d in GameObject.FindObjectsOfType<SPLOM3D>())
            {
                if (splom3d.BaseVisualization == vis)
                {
                    Destroy(splom3d.gameObject);
                }
            }
        }
    }

    public void RemoveVisualisation(Visualization visualization)
    {
        foreach (Axis axis in visualization.axes)
        {
            RemoveAxis(axis);
        }
    }

    public void CreateVisualisationAndAnimateToHand(GameObject interactingController, Vector3 startPos, float time)
    {
        GameObject visualisation = CreateVisualisation();

        AnimateGameObjectToHand(visualisation, interactingController, startPos, time);
    }

    public GameObject CreateVisualisation()
    {
        string x = null;
        string y = null;
        string z = null;
        switch (ScreenManager.Instance.ChartType)
        {
            case SandDanceChartType.Column:
                x = ScreenManager.Instance.ChartX;
                break;

            case SandDanceChartType.Density:
                x = ScreenManager.Instance.ChartX;
                y = ScreenManager.Instance.ChartY;
                break;

            case SandDanceChartType.Scatter:
                x = ScreenManager.Instance.ChartX;
                y = ScreenManager.Instance.ChartY;
                z = ScreenManager.Instance.ChartColorBy;
                break;

            default:
                return null;
        }

        return CreateCustomVisualisation(x, y, z);
    }

    public GameObject CreateCustomVisualisation(string x, string y, string z)
    {
        int? xDimension = x != null ? dataObject.dimensionToIndex(x) : (int?)null;
        int? yDimension = y != null ? dataObject.dimensionToIndex(y) : (int?)null;
        int? zDimension = z != null ? dataObject.dimensionToIndex(z) : (int?)null;

        return CreateCustomVisualisation(xDimension, yDimension, zDimension);
    }

    public GameObject CreateCustomVisualisation(int? x, int? y, int? z)
    {
        GameObject parent = new GameObject();
        parent.AddComponent<VisualisationContainer>();
        parent.tag = "VisualisationContainer";
        Rigidbody rb = parent.AddComponent<Rigidbody>();
        rb.useGravity = false;
        rb.isKinematic = true;
        rb.collisionDetectionMode = CollisionDetectionMode.ContinuousDynamic;

        List<int> indicesToSet = new List<int>(ScreenManager.Instance.SelectedIndices);
        if (indicesToSet.Count == 0)
        {
            indicesToSet = Enumerable.Range(0, dataObject.DataPoints).ToList();
        }

        Coroutine setIndices = null;

        if (x != null)
        {
            GameObject xAxis = CreateAxis((int)x);
            xAxis.transform.SetParent(parent.transform);
            xAxis.transform.localPosition = new Vector3(0, -0.14f, 0);
            Vector3 xRotation = Vector3.zero;
            xRotation.x = 180;
            xRotation.y = 180;
            xRotation.z = -90;
            xAxis.transform.rotation = Quaternion.Euler(xRotation);

            if (setIndices == null)
                setIndices = StartCoroutine(SetDisplayedIndices(xAxis.GetComponent<Axis>(), indicesToSet));
        }
        if (y != null)
        {
            GameObject yAxis = CreateAxis((int)y);
            yAxis.transform.SetParent(parent.transform);
            yAxis.transform.localPosition = new Vector3(0.14f, 0, 0);
            Vector3 yRotation = Vector3.zero;
            yRotation.y = 180;
            yAxis.transform.rotation = Quaternion.Euler(yRotation);

            if (setIndices == null)
                setIndices = StartCoroutine(SetDisplayedIndices(yAxis.GetComponent<Axis>(), indicesToSet));
        }
        if (z != null)
        {
            GameObject zAxis = CreateAxis((int)z);
            zAxis.transform.SetParent(parent.transform);
            zAxis.transform.localPosition = new Vector3(0.14f, -0.14f, -0.14f);
            Vector3 zRotation = Vector3.zero;
            zRotation.x = 90;
            zRotation.y = -90;
            zRotation.z = 90;
            zAxis.transform.rotation = Quaternion.Euler(zRotation);

            if (setIndices == null)
                setIndices = StartCoroutine(SetDisplayedIndices(zAxis.GetComponent<Axis>(), indicesToSet));
        }
        
        return parent;
    }

    IEnumerator SetDisplayedIndices(Axis axis, List<int> displayedIndices)
    {
        yield return new WaitForEndOfFrame();
        yield return new WaitForEndOfFrame();

        foreach (Visualization vis in axis.correspondingVisualizations())
        {
            vis.DisplayedIndices = displayedIndices;
        }
    }

    public void TransferAxis(Axis axis, Dimension dimension)
    {
        WebSocketManager.Instance.SetDimension(axis.axisName, dimension);
    }

    public void TransferVisualisation(Visualization vis)
    {
        List<Axis> axes = vis.axes;
        List<int> displayedIndices = vis.DisplayedIndices;

        // Set the screen appropriately based on the number of axes in the visualisation
        if (axes.Count == 1)
        {
            WebSocketManager.Instance.SetVisualisationAsColumn(axes[0].axisName);

            int[] filtered = GetFilteredIndices(axes[0].axisName, axes[0].MinFilter, axes[0].MaxFilter);
            filtered = filtered.Intersect(displayedIndices).ToArray();
            WebSocketManager.Instance.SetFilteredIndices(filtered);
        }
        else if (axes.Count == 2)
        {
            WebSocketManager.Instance.SetVisualisationAsScatter(axes[0].axisName, axes[1].axisName);

            int[] xFiltered = GetFilteredIndices(axes[0].axisName, axes[0].MinFilter, axes[0].MaxFilter);
            int[] yFiltered = GetFilteredIndices(axes[1].axisName, axes[1].MinFilter, axes[1].MaxFilter);

            int[] filtered = xFiltered.Intersect(yFiltered).ToArray();
            filtered = filtered.Intersect(displayedIndices).ToArray();
            WebSocketManager.Instance.SetFilteredIndices(filtered);
        }
        else if (axes.Count == 3)
        {
            WebSocketManager.Instance.SetVisualisationAsColouredScatter(axes[0].axisName, axes[1].axisName, axes[2].axisName);

            int[] xFiltered = GetFilteredIndices(axes[0].axisName, axes[0].MinFilter, axes[0].MaxFilter);
            int[] yFiltered = GetFilteredIndices(axes[1].axisName, axes[1].MinFilter, axes[1].MaxFilter);
            int[] zFiltered = GetFilteredIndices(axes[2].axisName, axes[2].MinFilter, axes[2].MaxFilter);

            int[] filtered = xFiltered.Intersect(yFiltered).Intersect(zFiltered).ToArray();
            filtered = filtered.Intersect(displayedIndices).ToArray();
            WebSocketManager.Instance.SetFilteredIndices(filtered);
        }
        else
        {
            // If it has more than 3 axes, don't do anything
            return;
        }
    }

    public List<Axis> DuplicateVisualisation(Visualization vis)
    {
        List<Axis> newAxes = new List<Axis>();

        foreach (Axis axis in vis.axes)
        {
            GameObject newAxis = CreateAxis(axis.axisId);            
            newAxis.transform.position = axis.transform.position;
            newAxis.transform.rotation = axis.transform.rotation;
            newAxes.Add(newAxis.GetComponent<Axis>());
        }

        StartCoroutine(SetDisplayedIndices(newAxes[0], vis.DisplayedIndices)); 
        return newAxes;
    }

    public int[] GetFilteredIndices(string name, float min, float max)
    {
        float[] array = dataObject.getDimension(name);

        List<int> indices = new List<int>();

        for (int i = 0; i < array.Length; i++)
        {
            float value = array[i] - 0.5f;

            if (min <= value && value <= max)
            {
                indices.Add(i);
            }
        }

        return indices.ToArray();
    }


    //
    // Debug functions
    //

    void CreateSPLOMS()
    {
        print("creating the splom");
        Axis[] axes = (Axis[])GameObject.FindObjectsOfType(typeof(Axis));

        GameObject a = axes[0].gameObject;// GameObject.Find("axis horesepower");
        a.transform.position = new Vector3(0f, 1.383f, 0.388f);

        Quaternion qt = new Quaternion();
        qt.eulerAngles = new Vector3(-90f, 180f, 0f);
        a.transform.rotation = qt;

        GameObject b = axes[1].gameObject;
        b.transform.position = new Vector3(0f, 1.506231f, 0.2461f);

        GameObject d = axes[2].gameObject;
        d.transform.position = new Vector3(0.1485f, 1.4145f, 0.2747f);
        qt.eulerAngles = new Vector3(0f, 180f, 90f);
        d.transform.rotation = qt;
    }

    void CreateLSPLOM()
    {
        Quaternion qt = new Quaternion();
        qt.eulerAngles = new Vector3(0f, 180f, 90f);

        GameObject a = GameObject.Find("axis horesepower");
        a.transform.position = new Vector3(0.1018f, 1.369f, -1.3629f);
        a.transform.rotation = qt;

        GameObject b = GameObject.Find("axis weight");
        b.transform.position = new Vector3(-0.04786599f, 1.506231f, -1.356f);

        GameObject c = GameObject.Find("axis mpg");
        c.transform.position = new Vector3(-0.045f, 1.768f, -1.357f);

        GameObject d = GameObject.Find("axis name");
        d.transform.position = new Vector3(-0.047f, 2.03f, -1.354f);

        qt.eulerAngles = new Vector3(0f, 180f, 90f);

        GameObject e = GameObject.Find("axis displacement");
        e.transform.position = new Vector3(0.37f, 1.378f, -1.37f);
        e.transform.rotation = qt;

    }

    void CreateSPLOMCenter()
    {
        Quaternion qt = new Quaternion();
        qt.eulerAngles = new Vector3(0f, 180f, -90f);

        GameObject c = GameObject.Find("axis mpg");
        c.transform.position = new Vector3(1.3173f, 1.7632f, -0.941f);

        GameObject d = GameObject.Find("axis weight");

        d.transform.position = new Vector3(1.173f, 1.6389f, -0.9362f);
        d.transform.rotation = qt;

        //        Quaternion qt = new Quaternion();

        GameObject e = GameObject.Find("axis displacement");

        e.transform.position = new Vector3(0.8942f, 1.64f, -0.938f);
        e.transform.rotation = qt;
    }

    void CreateSPLOMSWithU()
    {
        Axis[] axes = (Axis[])GameObject.FindObjectsOfType(typeof(Axis));
        for (int i = 2; i < 8; ++i)
        {
            axes[i].transform.Translate(i % 2 * (axes[i].transform.localScale.x * 10f), i % 2 * (-axes[i].transform.localScale.x * 6.5f), 1f);
            axes[i].transform.Rotate(0f, 0f, i % 2 * (90f));
        }

        GameObject a = GameObject.Find("axis mpg");
        a.transform.position = new Vector3(0.236f, 1.506231f, -1.486f);
    }
}
