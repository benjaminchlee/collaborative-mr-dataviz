﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DisplayMirror : MonoBehaviour {

    public string CameraName = "OBS-Camera";

    void Start()
    {
        WebCamTexture webcamTexture = new WebCamTexture();
        webcamTexture.deviceName = CameraName;
        Renderer renderer = GetComponent<Renderer>();
        renderer.material.mainTexture = webcamTexture;

        try
        {
            webcamTexture.Play();
            Debug.Log("Rendering from camera " + CameraName);
        }
        catch
        {
            throw new System.Exception("Could not find " + CameraName + ". Please check that it is currently running and that the appropriate registry tweak has been applied (see README.md).");
        }

    }
}
