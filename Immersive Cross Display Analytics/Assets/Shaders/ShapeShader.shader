﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "Custom/Shape Shader" {
	Properties{
		_UnselectedColor("Unselected Color", Color) = (125, 125, 125, 0.1)
		_SelectedColor("Selected Color", Color) = (255, 255, 0, 0.25)
	}

	SubShader{
		Tags {"RenderType" = "Transparent"  "Queue" = "Transparent"}

		// Render into depth buffer only
		Pass {
			ColorMask 0
		}

		Pass {
			// Don't render pixels whose value in the stencil buffer equals 1.
			Stencil {
			  Ref 1
			  Comp NotEqual
			}

			ZWrite Off
			Blend SrcAlpha OneMinusSrcAlpha
			Cull Off
			LOD 100

			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag

			fixed4 _UnselectedColor;
			fixed4 _SelectedColor;

			struct VertexInput {
				float4 v : POSITION;
				float4 color: COLOR;
				float3 normal: NORMAL;
			};

			struct VertexOutput {
				float4 pos : SV_POSITION;
				float4 col : COLOR;
				float3 normal: NORMAL;
			};

			VertexOutput vert(VertexInput v) {

				VertexOutput o;
				o.pos = UnityObjectToClipPos(v.v);
				o.col = v.color;
				o.normal = v.normal;

				return o;
			}

			float4 frag(VertexOutput o) : COLOR {
				if (o.normal.x > 0) {
					return _SelectedColor;
				}
				else {
					return _UnselectedColor;
				}
			}

			ENDCG
		}
	}
}